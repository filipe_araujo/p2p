//----------------------------------------------------------------------------------------------------------------//
// Exportacao dos dados de uma tela Modelo2 para o Excel.
//----------------------------------------------------------------------------------------------------------------//
User Function TExcel1()

Local aHeader := {}
Local aCols   := {}
Local cNome   := "RENATA" //COLOQUE AQUI O NOME DA CLIENTE

If !ApOleClient("MSExcel")
   MsgAlert("Microsoft Excel n�o instalado!")
   Return
EndIf

dbSelectArea("SX3")
dbSetOrder(1)
dbSeek("SZB")

While SX3->X3_Arquivo == "SZB" .And. !SX3->(EOF())

   If X3Uso(SX3->X3_Usado)    .And.;
      cNivel >= SX3->X3_Nivel .And.;
      Trim(SX3->X3_Campo) $ "ZB_ITEM/ZB_TIPO/ZB_HIST/ZB_VALOR"

      AAdd(aHeader, {Trim(SX3->X3_Titulo),;
                     SX3->X3_Campo       ,;
                     SX3->X3_Picture     ,;
                     SX3->X3_Tamanho     ,;
                     SX3->X3_Decimal     ,;
                     SX3->X3_Valid       ,;
                     SX3->X3_Usado       ,;
                     SX3->X3_Tipo        ,;
                     SX3->X3_Arquivo     ,;
                     SX3->X3_Context})
		
   EndIf
	
   SX3->(dbSkip())
	
End

dbSelectArea("SZB")
dbSetOrder(1)
If !dbSeek(xFilial("SZB") + cNome)
   MsgAlert("Nome nao encontrado!")
   Return
EndIf

While Trim(SZB->ZB_NOME) == cNome .And. !SZB->(Eof())
   AAdd(aCols, {SZB->ZB_ITEM, SZB->ZB_HIST, SZB->ZB_TIPO, SZB->ZB_VALOR, .F.})
   SZB->(dbSkip())
End

dbSelectArea("SZA")
dbSetOrder(1)
If !dbSeek(xFilial("SZA") + cNome)
   MsgAlert("Nome nao encontrado!")
  Return
EndIf

DlgToExcel({ {"CABECALHO", "Tabela de Saldos", {"Numero","Cliente","Saldo"}, {SZA->ZA_NUMERO,SZA->ZA_NOME,SZA->ZA_SALDO}}, {"GETDADOS", "Movimentacoes", aHeader, aCols} })

// Ou, so a parte da GetDados: DlgToExcel({ {"GETDADOS", "TESTE", aHeader, aCols} })

Return

//----------------------------------------------------------------------------------------------------------------//
// Exportacao de dados para o Excel.
//----------------------------------------------------------------------------------------------------------------//
User Function TExcel2()

Local aCabec := {}
Local aDados := {}

If !ApOleClient("MSExcel")
   MsgAlert("Microsoft Excel n�o instalado!")
   Return
EndIf

aCabec := {"Codigo", "Nome", "Endereco"}

dbSelectArea("SA1")
dbGoTop()

While !SA1->(Eof())

   AAdd(aDados, {SA1->A1_Cod, SA1->A1_Nome, SA1->A1_End})

   SA1->(dbSkip())

End

DlgToExcel({ {"ARRAY", "Exportacao para o Excel", aCabec, aDados} })

Return

//----------------------------------------------------------------------------------------------------------------//
// Exportacao de dados para o Excel.
//----------------------------------------------------------------------------------------------------------------//
User Function TExcel3()

Local oExcel
Local cArq
Local nArq
Local cPath

If !ApOleClient("MSExcel")
   MsgAlert("Microsoft Excel n�o instalado!")
   Return
EndIf

cArq  := CriaTrab(Nil, .F.)
cPath := GetSrvProfString("ROOTPATH", "C:\") + "\SYSTEM\"
nArq  := FCreate(cPath + cArq + ".CSV")

If nArq == -1
   MsgAlert("Nao conseguiu criar o arquivo!")
   Return
EndIf

FWrite(nArq, "Codigo;Nome;Endereco" + Chr(13) + Chr(10))

dbSelectArea("SA1")
dbGoTop()
While !SA1->(Eof())
   FWrite(nArq, SA1->A1_Cod + ";" + SA1->A1_Nome + ";" + SA1->A1_End + Chr(13) + Chr(10))
   SA1->(dbSkip())
End

FClose(nArq)

oExcel := MSExcel():New()
oExcel:WorkBooks:Open(cPath + cArq + ".CSV")
oExcel:SetVisible(.T.)
oExcel:Destroy()

FErase(cPath + cArq + ".CSV")

Return
   
USer function TExcel4()
      
	If !ApOleClient("MSExcel")
	   MsgAlert("Microsoft Excel n�o instalado!")
	   Return
	EndIf
	
	cArq  := "SX2010"
	cPath := GetSrvProfString("ROOTPATH", "C:\") + "\SYSTEM\"
	
	oExcel := MSExcel():New()
	oExcel:WorkBooks:Open(cPath + cArq + ".DBF")
	oExcel:SetVisible(.T.)

Return