//----------------------------------------------------------------------------------------------------------------// 
// Demonstracao da construcao de telas e seus componentes.
//
// Menu POPUP. *** S� funciona se executar via Remote ***
//----------------------------------------------------------------------------------------------------------------// 

#Include "PROTHEUS.CH"

User Function TstTela1()

Local oDlg
Local oBtnOk
Local oBtnCancel
           
Define MSDialog oDlg Title "Cadastro" From 0,0 To 425,450 Pixel

@20,10 Say "Teste" Pixel Of oDlg

@oDlg:nHeight/2-30,oDlg:nClientWidth/2-70 Button oBtnOk     Prompt "Menu"     Size 30,15 Pixel Action Confirma(oDlg)      Of oDlg
@oDlg:nHeight/2-30,oDlg:nClientWidth/2-35 Button oBtnCancel Prompt "Cancelar" Size 30,15 Pixel Action oDlg:End() Cancel Of oDlg

Activate MSDialog oDlg Centered

Return Nil

//----------------------------------------------------------------------------------------------------------------// 
Static Function Confirma(oDlg)

Local oMenu

SaveInter()

MENU oMenu POPUP
 MENUITEM "Op��o 1" Action MsgAlert("Op��o 1")
 MENUITEM "Op��o 2" Action MsgAlert("Op��o 2")
 MENUITEM "Op��o 3" Action MsgAlert("Op��o 3")
 MENUITEM "Fecha"   Action oMenu:End()
ENDMENU

If SetMDIChild()
   oMenu:Activate(200,200,oMainWnd)
   //oMenu:Activate(200,200,oDlg)
 Else
   oMenu:Activate(82,140,oMainWnd)
   //oMenu:Activate(82,140,oDlg)
EndIf
 
RestInter()

Return
