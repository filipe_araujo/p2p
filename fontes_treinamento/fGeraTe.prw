#Include "protheus.CH"
#Include "TOPCONN.CH"
User Function fGeraTe()
Local oDlg, oVar 
Local cVar1 := criavar("A1_COD")
Local nVar2 := 0
Local nVar3 := 0
Local nVar4 := 0

	Define MSDialog oDlg Title OemToAnsi("Calcula o Quadrado") From 0,0 To 180,380 Pixel
	
		@03,10 To 70,180 Pixel
		
		@ 10,20 Say "Codigo Cliente" Pixel Of oDlg
		@ 10,50 MSGet oVar Var cVar1 F3 "SA1" Size 50,10 Pixel  Of oDlg
		@ 25,20 Say "Numero 2" Pixel Of oDlg
		@ 25,50 MSGet oVar Var nVar2 Picture "@E 999,999.99" Size 50,10 Pixel  Of oDlg
		@ 40,20 Say "Numero 3" Pixel Of oDlg
		@ 40,50 MSGet oVar Var nVar3 Picture "@E 999,999.99" Size 50,10 Pixel  Of oDlg
		@ 55,20 Say "Numero 4" Pixel Of oDlg
		@ 55,50 MSGet oVar Var nVar4 Picture "@E 999,999.99" Size 50,10 Pixel  Of oDlg
		
		@75,20 Button oBtnOk     Prompt "&Ok"       Size 30,15 Pixel; 
		       Action U_quadrado({nVar1,nVar2,nVar3,nVar4}) Of oDlg
		@75,80 Button oBtnCancel Prompt "&Cancelar" Size 30,15 Pixel ;
		       Action ( oDlg:End()) Cancel Of oDlg
		
	Activate MSDialog oDlg Centered

Return 

