//----------------------------------------------------------------------------------------------------------------//
// Demonstracao do For...Next.
//----------------------------------------------------------------------------------------------------------------//

User Function TstFor()

Local i

For i := 1 To 10
    MsgAlert(i)
Next

Return

//----------------------------------------------------------------------------------------------------------------//
User Function TstFor1()

Local i
Local nIni, nFim

nIni := 100
nFim := 120

For i := nIni To nFim Step 2
    MsgAlert(i)
    If i > 110
       Exit      // Break tambem encerra.
    EndIf
Next

Return

//----------------------------------------------------------------------------------------------------------------//
User Function TstFor2()

Local i
Local nIni, nFim

nIni := 1
nFim := 10

For i := nFim To nIni Step -1
    MsgAlert(i)
Next

Return

//----------------------------------------------------------------------------------------------------------------//
User Function TstFor3()

Local i
Local j

For i := 20 To 25
    MsgAlert("i=" + Str(i))
    For j := 1 To 5
        MsgAlert("i=" + AllTrim(Str(i)) + "   j=" + AllTrim(Str(j))) 
    Next 
    
    If MsgYesNo("Abortar?")  
    	Exit
    EndIf
Next

Return                                          


//----------------------------------------------------------------------------------------------------------------//
User Function TstFor4()

Local i
nIni := 100
nFin := 120

For i := nIni To nFin
    Alert(i)
    If i > (nIni + nFin) / 2     // Encerra qdo. atingir a metade do intervalo.
       Break
    EndIf
Next

Return


//----------------------------------------------------------------------------------------------------------------//
User Function TstFor5()

Local aFunc := {{"Leandro",10000,0.7},{"Lucas",1000,0.1},{"Pedro",4000,0.7},{"Marcos",6000,0.7},{"Julio",7000,0.7}}

For i := 1 To Len(aFunc)

	Alert(Len(aFunc[i]))
	
	If aFunc[i][2]>5000
		MsgAlert("Nome: "+aFunc[i][1]+;
		         " - Salario: "+Alltrim(Str(aFunc[i][2]))+;
		         " - Aumento: "+Alltrim(Str(aFunc[i][2]*aFunc[i][3]))+;
		         " - Sal+Aumento: "+Alltrim(Str( (aFunc[i][2]*aFunc[i][3]) + aFunc[i][2])))		
	EndIf

Next  

//5 FUNCIONARIOS
//AUMENTO SOMENTE PARA AQUELES COM SALARIO SUPERIOR A 5000,00
//MOSTRAR SALARIO + AUMENTO
//CONCATENAR TODAS AS INFORMACOES
//   Ex: Nome: Leandro - Salari: 10.000,00 - Aumento: 1.000 - Sal+Aumento: 11.000

Return