#Include "APVT100.CH"
#Include "TbiConn.ch"

//----------------------------------------------------------------------------//
// Programa de exemplo das funcoes do micro-terminal.
//----------------------------------------------------------------------------//
User Function VTDEMO()

Local nI, nPos  := 1        
Local cOpcao    := ""
Local cCodigo   := Space(6)
Local dData     := CtoD("")
Local cPSW      := Space(6)
Local nValor    := 0
Local aOpcoes
Local aItens, aCab, aSize 
Local aFields, aHeader

Prepare Environment Empresa "99" Filial "01" Modulo "ESP" Tables "SB1"

While .T.

   VTClear()            // Apaga a tela e coloca o cursor na posi��o inicial.

   cCodigo := Space(6)
   dData   := CtoD("")
   nValor  := 0                 
   cPSW	   := Space(6)

   @00,00 VTSay "Demo de VT100"             // Mostra uma string na tela.
   @01,00 VTSay "Modelo: " + VTModelo()     // Mostra o modelo do micro-terminal.

   If VTLastKey() == 27                     // Verifica se a ultima tecla pressionada � ESC.
      Exit
   EndIf

   VTInkey(0)           // Faz uma pausa, aguarda o pressionamento de uma tecla e retorna o seu codigo.
   VTClear()            // Apaga a tela e coloca o cursor na posi��o inicial.
   VTClearBuffer()      // Limpa o buffer do teclado.

   @00,00 VTSay "Codigo: "
   @00,08 VTGet cCodigo Pict "@!" Valid ValidCod(cCodigo) When WhenCod()
   VTRead  

   If VTLastKey() == 27
      Loop
   EndIf

   @01,00 VTSay "Data: "
   @01,07 VTGet dData Pict "99/99/99" Valid ValidData(dData) When WhenData()
   VTRead

   If VTLastKey() == 27
      Loop
   EndIf

   VTClear()
   VTClearBuffer()

   @00,00 VTSay "Valor: "
   @00,07 VTGet nValor Pict "@E 999.99"  When WhenValor()
   VTRead       

   If VTLastKey() == 27
      Loop
   EndIf

   @01,00 VTSay "Senha: "
   @01,07 VTGet cPSW PASSWORD Valid (VTAlert(cPSW, "Senha Digitada", .T., 1500), .T.) When WhenSenha()
   VTRead 

   If VTLastKey() == 27
      Loop
   EndIf
   
   VTClear()                                
   VTAlert("Entrando no AChoice....", "ATENCAO!", .T., 1500)
   VTClear()

   VTClearBuffer()

   aOpcoes := {"Opcao 1",;
               "Opcao 2",;
               "Opcao 3",;
               "Opcao 4",;
               "Opcao 5"}      

   nPos	:= VTAChoice(0, 0, VTMaxRow(), VTMaxCol(), aOpcoes, , "U_VldAchVT", nPos)   

   VTAlert("Opcao selecionada: " + LTrim(Str(nPos)), "", .T., 2000)
  	
   VTAlert("Entrando no aBrowser... aguarde...", "ATENCAO!", .T., 1500)  	

   aItens :={{"1010",  10, "DESCRICAO1", "UN"},;
             {"2010",  20, "DESCRICAO2", "CX"},;         
             {"2020",  30, "DESCRICAO3", "CX"},;                   
             {"2010",  40, "DESCRICAO4", "CX"},;         
             {"2020",  50, "DESCRICAO5", "CX"},;                   
             {"3010",  60, "DESCRICAO6", "CX"},;         
             {"3020",  70, "DESCRICAO7", "CX"},;
             {"3030",  80, "DESCRICAO7", "CX"},;
             {"3040",  90, "DESCRICAO7", "CX"},;          
             {"2010", 100, "DESCRICAO4", "CX"},;                   
             {"2010", 110, "DESCRICAO4", "CX"},;         
             {"2020", 120, "DESCRICAO5", "CX"},;                   
             {"3010", 130, "DESCRICAO6", "CX"},;         
             {"3020", 140, "DESCRICAO7", "CX"},;
             {"3030", 150, "DESCRICAO7", "CX"},;
             {"3050", 160, "DESCRICAO7", "CX"}}
          
   aCab  := {"Codigo", "Cod", "Descricao", "UM"}
   aSize := {10, 6, 20, 10}
   nPos  := 1
   nPos  := VTaBrowse(,,,, aCab, aItens, aSize, , nPos)
   VTAlert("Item selecionado: " + LTrim(Str(nPos)), "ATENCAO!", .T., 2000)
	
   VTAlert("Entrando no DBBrowser... aguarde...", "ATENCAO!", .T., 2000)
   VTClear()
   aFields := {"B1_COD","B1_DESC", "B1_UM", "B1_PICM"}
   aSize   := {16,20,10,15}          
   aHeader := {"COD", "DESCRICAO     ", "UM", "% ICM"}       
   dbSelectArea("SB1")
   SB1->(dbSeek(xFilial()))
   nPos := VTDBBrowse(,,,, "SB1", aHeader, aFields, aSize)
   VTAlert("Item selecionado: " + LTrim(Str(nPos)), "ATENCAO!", .T., 2000)
             
   VTClearBuffer()
   If VTYesNo("Deseja finalizar?", "PERGUNTA")
      Exit
   EndIf
  
   If VTLastKey() == 27
      Loop
   EndIf

   VTClearBuffer()

EndDo

Return .T.

//----------------------------------------------------------------------------//
Static Function ValidCod(cCodigo)

Local aTela

VTSAVE SCREEN TO aTela       // Salva a tela.
VTAlert("Total de bytes: " + AllTrim(Str(Len(AllTrim(cCodigo)))), "Validando:", .T., 2000)
VTClear()
VTRESTORE SCREEN FROM aTela  // Restaura a tela.

Return .T.

//----------------------------------------------------------------------------//
Static Function WhenCod()

Local aTela

VTSAVE SCREEN TO aTela
VTAlert("Exemplo de Get" + Chr(13) + Chr(10) + "com Caracter", "ATENCAO!", .T., 2000)
VTClear()
VTRESTORE SCREEN FROM aTela

Return .T.

//----------------------------------------------------------------------------//
Static Function WhenData()

Local aTela

VTSAVE SCREEN TO aTela
VTAlert("Exemplo de Get" + Chr(13) + Chr(10) + "com Data", "ATENCAO!", .T., 2000)
VTClear()
VTRESTORE SCREEN FROM aTela

Return .T.

//----------------------------------------------------------------------------//
Static Function WhenValor()

Local aTela

VTSAVE SCREEN TO aTela
VTAlert("Exemplo de Get" + Chr(13) + Chr(10) + "com Numerico", "ATENCAO!", .T., 2000)
VTClear()
VTRESTORE SCREEN FROM aTela

Return .T.

//----------------------------------------------------------------------------//
Static Function WhenSenha()

Local aTela

VTSAVE SCREEN TO aTela
VTAlert("Exemplo de Get" + Chr(13) + Chr(10) + "com Senha", "ATENCAO!", .T., 2000)
VTClear()
VTRESTORE SCREEN FROM aTela

Return .T.

//----------------------------------------------------------------------------//
Static Function ValidData(dData)

Local lRet := .T.

If Empty(dData)
   lRet := .F.
   VTAlert("Data invalida", "Atencao", .T., 2000)
EndIf

Return lRet
                                   
//----------------------------------------------------------------------------//
User Function VldAchVT(nModo, nElem, nElemW)

If nModo == 1 
   VTAlert("Inicio do AChoice", "AChoice", .T., 1000)
ElseIf nModo == 2 
   VTAlert("Fim do AChoice", "AChoice", .T., 1000)
Else
   If VTLastkey() == 27 
      VTAlert("Saindo do AChoice", "AChoice", .T., 1000)
      VTBeep(1)
      Return 0  
   ElseIf VTLastkey() == 13      
      VTAlert("Tecla <ENTER> pressionada", "AChoice", .T., 1000)
      VTBeep(1)
      Return 1          
   EndIf      
EndIf        

Return 2
