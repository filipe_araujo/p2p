
// Rotina de relatorio que imprime o extrato de contas-correntes.
// Similar ao REL001, porem, utilizando os dois arquivos: SZ1 e SZ2.

User Function Rel002()

Local cAlias   := ""                                  // Alias do arquivo a ser impresso.
Local cNomeArq := FunName()                           // Nome do arquivo a ser gerado caso a impressao seja em disco.
Local wnRel                                           // Retorno da fun��o SetPrint().
// Descricao do relatorio.
Local cDesc1   := "Este programa imprime o extrato das contas-correntes de todas as Contas."
Local cDesc2   := ""
Local cDesc3   := ""

Private cTitulo   := "Extrato de Contas-Correntes"     // Titulo do relatorio.
Private cCabec1   := "  Data    Tipo Mov           Valor           Saldo"
Private cCabec2   := ""
Private cNomeProg := FunName()                        // Nome do programa no cabecalho do relatorio.
Private cTamanho  := "P"                              // Tamanho do relatorio.
Private nTipo                                         // Relatorio normal ou comprimido.
Private m_Pag     := 1                                // Numero da pagina.

// O array aReturn � usado pela fun��o SetPrint(). Define o formato, tipo de impressao e o nome do arquivo.
// aReturn[1] = Reservado para formulario
// aReturn[2] = Reservado para numero de vias
// aReturn[3] = Destinatario
// aReturn[4] = Formato: 1-Retrato, 2-Paisagem
// aReturn[5] = Tipo midia: 1-Disco, 2-Via spool, 3-Direto na porta, 4-EMail
// aReturn[6] = "NomeArq"-Disco, "LPT1"-Via spool, "LPT1"-Direto na porta, ""-Cancelado
// aReturn[7] = Expressao do filtro
// aReturn[8] = Ordem a ser selecionada
Private aReturn := {"Zebrado", 1,"Administracao", 1, 1, "CANCELADO", "", 1}    
Private cPerg   := FunName()

// Fun��o SetPrint(): prepara os parametros para a impressao do relatorio.
// Parametros:
//    cAlias   --> Alias do arquivo a ser listado. Se passado, apresenta as op��es de sele��o dos campos
//                 a imprimir (Dicionario de Dados) e defini��o de um filtro.
//    cNomeArq --> Nome do arquivo a ser gerado caso selecione a op��o de impress�o em disco.
//    cPerg    --> Codigo do conjunto de perguntas (parametros). Caso seja passado, permite apresentar o
//                 conjunto de perguntas.
//    cTitulo  --> Titulo do relatorio.
//    cDesc1   --> Descri��o do relatorio.
//    cDesc2   --> Descri��o do relatorio.
//    cDesc3   --> Descri��o do relatorio.
//    lDic     --> .T. apresenta o Dicionario de Dados para sele��o dos campos a imprimir. .F. nao apresenta.
//    aOrd     --> Array com as ordens de impressao.
//    lCompres --> .T. comprimido. .F. normal.
//    cSize    --> Tamanho do relatorio: "P"-80 col., "M"-132 col., "G"-220 col.
//
// Retorna para a variavel wnRel o nome do arquivo a ser gerado, caso a impressao seja em disco.
CriaSx1(cPerg)

wnRel := SetPrint(cAlias, cNomeArq, cPerg, @cTitulo, cDesc1, cDesc2, cDesc3, .F., , .F., cTamanho)

// Define se o relatorio � normal ou comprimido.
nTipo := IIf(aReturn[4] == 1, 15, 18)

// Se na fun��o SetPrint() foi clicado o botao Cancelar, ou seja, abortou a emiss�o do relatorio,
// o sexto elemento do vetor aReturn conter� "CANCELADO".
If aReturn[6] <> "CANCELADO"           // Nao cancelou o relatorio.
   // Prepara a impressora ou o arquivo para receber o relatorio.
   // Estabelece os campos a serem impressos e o filtro, caso tenham sido definidos.
   SetDefault(aReturn, cAlias)
   // Executa a rotina de impressao do relatorio.
   RptStatus({|lFim| Imprime(@lFim)}, "Aguarde...", "Imprimindo o extrato...")
   // Envia o relatorio para o spool ou exibe na tela, dependendo da op��o selecionada.
   OurSpool(wnRel)
EndIf

MS_Flush()                             // Libera a memoria.

Return Nil

//----------------------------------------------------------------------------------------------------------------// 
// Rotina de impress�o do relatorio.
//----------------------------------------------------------------------------------------------------------------// 
Static Function Imprime(lFim)

Local nValor
Local nSaldo

dbSelectArea("SZA")          // Seleciona o Cadastro de Contas.
dbOrderNickName("NOME")      // Seleciona a chave "Filial + Nome".
//dbGoTop()                    // Vai para o primeiro registro, de acordo com a ordem selecionada.
If !dbSeek(xFilial("SZA") + mv_Par01)
  MsgAlert("Nome nao encontrado!")
  Return
EndIf

SetRegua(RecCount())         // Inicializa a regua.

While !SZA->(Eof()) .AND. SZA->ZA_NOME==mv_Par01          // Executa a sequencia de comandos enquanto nao for Fim de Arquivo.

   IncRegua()                // Incrementa a regua.

   If lFim
      @PRow()+2,000 PSay "*** CANCELADO PELO USUARIO ***"
      Exit
   EndIf

   If PRow() > 60            // Se o "cursor" da impressora ultrapassou 60 linhas,
      Eject                  // salta para a pagina seguinte e
      SetPrc(0, 0)           // zera o "cursor" da impressora.
   EndIf

   If PRow() == 0            // Se o "cursor" da impressora estiver no inicio de uma nova pagina,
      Cabec(cTitulo, cCabec1, cCabec2, cNomeProg, cTamanho, nTipo)  // imprime o cabe�alho.
   EndIf

   // Para cada Conta, deve haver uma separa��o e o seu nome deve aparecer antes da listagem de suas transa��es.
   // E tamb�m o seu saldo deve ser inicializado.

   @PRow()+1,000 PSay __PrtThinLine()              // Imprime uma linha para separar a Conta anterior.
   @PRow()+1,000 PSay "Nome: " + SZA->ZA_Nome      // Imprime o nome da nova Conta.
   @PRow()+1,000 PSay " "                          // Pula uma linha em branco.

   nSaldo := 0                                     // Inicializa o saldo.

   dbSelectArea("SZB")
   dbOrderNickName("NOME_NR_IT")
   dbSeek(xFilial("SZB") + SZA->ZA_Nome)

   While !SZB->(Eof()) .And. SZB->ZB_Nome == SZA->ZA_Nome

      If PRow() > 60         // Se o "cursor" da impressora ultrapassou 60 linhas,
         Eject               // salta para a pagina seguinte e
         SetPrc(0, 0)        // zera o "cursor" da impressora.
      EndIf

      If PRow() == 0         // Se o "cursor" da impressora estiver no inicio de uma nova pagina,
         Cabec(cTitulo, cCabec1, cCabec2, cNomeProg, cTamanho, nTipo)  // imprime o cabe�alho.
      EndIf

      @PRow()+1,000      PSay SZB->ZB_Data                                   // Imprime a Data.
      @PRow()  ,PCol()+2 PSay IIf(SZB->ZB_Tipo=="D", "Deposito", "Saque   ") // Imprime a descri��o do Tipo de Transa��o.

      // Se o Tipo de Transa��o for SAQUE, transforma o valor para negativo.
      nValor := SZB->ZB_Valor * IIf(SZB->ZB_Tipo=="D", 1, -1)

      // Acumula o saldo.
      nSaldo += nValor

      @PRow()  ,PCol()+2 PSay nValor Picture "@E 999,999,999.99"             // Imprime o Valor.
      @PRow()  ,PCol()+2 PSay nSaldo Picture "@E 999,999,999.99"             // Imprime o Saldo.

      SZB->(dbSkip())

   End

   dbSelectArea("SZA")
   SZA->(dbSkip())        // Vai para o proximo registro, tambem pela ordem selecionada.

End

// Imprime uma linha final, ap�s a ultima Conta listada.
@PRow()+1,000 PSay __PrtThinLine()

Return Nil         // Termina a fun��o, retornando ao ponto onde foi chamada. N�o retorna nenhum valor.    


//EXERCICIO
// IMPRIMIR LOGO A FRENTE DO NOME O SALDO CONTIDO NO CAMPO SZA->ZA_SALDO
// USAR MASCARA 999,999,999.99         

/*
EXEMPLO: 

--------------------------------------------------------------------------------
  Data    Tipo Mov           Valor           Saldo
--------------------------------------------------------------------------------
Nome: CARLOS                                 -105,00 <<NOVA IMFORMA��O
 
01/02/2012  Deposito          100,00          100,00
01/02/2012  Saque             -50,00           50,00
01/02/2012  Saque             -80,00          -30,00
01/02/2012  Saque             -10,00          -40,00
01/02/2012  Saque             -20,00          -60,00
01/02/2012  Saque             -30,00          -90,00
01/02/2012  Saque             -10,00         -100,00
01/02/2012  Saque              -5,00         -105,00
*/

Static Function CriaSx1(cPerg)

Local aHelp := {}

//            Texto do help em portugu�s        , ingl�s, espanhol
AAdd(aHelp, {{"Informe o nome da conta inicial"}, {""}, {""}})

PutSx1(cPerg,"01","Conta de?" ,"","","mv_ch1","C",20,00,00,"G","","SZA","","","mv_Par01","","","","","","","","","","","","","","","","",aHelp[1,1],aHelp[1,2],aHelp[1,3],"SZA")

Return