//----------------------------------------------------------------------------------------------------------------// 
// Demonstracao da construcao de telas e seus componentes.
//
// Botao que abre um menu POPUP. *** S� funciona se executar via Remote ***
//----------------------------------------------------------------------------------------------------------------// 

#Include "PROTHEUS.CH"

User Function TstTela2()

Local cAlias := "SZ2"

Private cCadastro := "Transa��o de Dep�sito ou Saque"
Private aRotina   := {}
Private aOpc

aOpc := { {"Incluir", "u_T2Man(1)", 0, 3},;
          {"Alterar", "u_T2Man(2)", 0, 4},;
          {"Excluir", "u_T2Man(3)", 0, 5} ;
        }

AAdd( aRotina, {"Pesquisar" , "AxPesqui", 0, 1} )
AAdd( aRotina, {"Visualizar", "AxVisual", 0, 2} )
AAdd( aRotina, {"Manuten��o", aOpc      , 0, 3} )

dbSelectArea(cAlias)
dbOrderNickName("NR_IT")

mBrowse(,,,,cAlias)

Return Nil

//----------------------------------------------------------------------------------------------------------------// 
User Function T2Man(nOpc)

If nOpc == 1
   MsgAlert("Inclusao")
 ElseIf nOpc == 2
   MsgAlert("Alteracao")
 Else
   MsgAlert("Exclusao")
EndIf

Return
 