#Include "APWEBEX.CH"
#Include "TbiConn.ch"

//----------------------------------------------------------------------------//
// Pagina para entrada de dados: Transa��o de Deposito/Saque.
//----------------------------------------------------------------------------//
User Function ASP5()

Local cHtml := ""

WEB EXTENDED INIT cHtml

cHtml += ExecInPage("275_ASP5")

WEB EXTENDED END

Return cHtml

//----------------------------------------------------------------------------//
User Function ASP5Grava()

Local cHtml := ""

WEB EXTENDED INIT cHtml

Prepare Environment Empresa "99" Filial "01" Modulo "ESP" Tables "SZ1", "SZ2"

dbSelectArea("SZ2")
RecLock("SZ2", .T.)
SZ2->Z2_Filial := xFilial("SZ2")
SZ2->Z2_Nome   := HttpPost->Nome
SZ2->Z2_Numero := GetSXENum("SZ2", "Z2_NUMERO")
SZ2->Z2_Item   := "01"
SZ2->Z2_Data   := CtoD(HttpPost->Data)
SZ2->Z2_Tipo   := HttpPost->Tipo
SZ2->Z2_Hist   := HttpPost->Hist
SZ2->Z2_Valor  := Val(HttpPost->Valor)
MSUnlock()

ConfirmSX8()

// Atualiza o saldo.
dbSelectArea("SZ1")
dbOrderNickName("NOME")
IF dbSeek(xFilial("SZ1") + SZ2->Z2_Nome)
	RecLock("SZ1", .F.)
	If SZ2->Z2_Tipo = "D"
	   SZ1->Z1_Saldo := SZ1->Z1_Saldo + SZ2->Z2_Valor
	 Else
	   SZ1->Z1_Saldo := SZ1->Z1_Saldo - SZ2->Z2_Valor
	EndIf
	MSUnLock()
	
	cHtml := "Transa��o gravada com sucesso!"
 eLSE
	cHtml := "O Nome escolhido n�o existe na Base de Dados!"
ENDIF
WEB EXTENDED END

Return cHtml
