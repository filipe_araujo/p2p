#INCLUDE "rwmake.ch"

User Function NOVO2

//� Declaracao de Variaveis                                             

Local cDesc1         := "Este programa tem como objetivo imprimir relatorio "
Local cDesc2         := "de acordo com os parametros informados pelo usuario."
Local cDesc3         := "Cadastro de produtos"
Local cPict          := ""
Local titulo       := "Cadastro de produtos"
Local nLin         := 80

Local Cabec1       := "C�digo           Descri��o                                       Um        Nipo" 
//"C�digo Descri��o                                            Um        Nipo"
//         1         2         3         4         5         6         7         8         9
//123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

Local Cabec2       := ""
Local imprime      := .T.
Local aOrd := {}
Private lEnd         := .F.
Private lAbortPrint  := .F.
Private CbTxt        := ""
Private limite           := 80
Private tamanho          := "P"
Private nomeprog         := "NOME" // Coloque aqui o nome do programa para impressao no cabecalho
Private nTipo            := 18
Private aReturn          := { "Zebrado", 1, "Administracao", 2, 2, 1, "", 1}
Private nLastKey        := 0
Private cbtxt      := Space(10)
Private cbcont     := 00
Private CONTFL     := 01
Private m_pag      := 01
Private wnrel      := "NOME" // Coloque aqui o nome do arquivo usado para impressao em disco

Private cString := "SB1"

dbSelectArea("SB1")
dbSetOrder(1)


//  Monta a interface padrao com o usuario...                            


wnrel := SetPrint(cString,NomeProg,"",@titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.T.,Tamanho,,.F.)

If nLastKey == 27
	Return
Endif

SetDefault(aReturn,cString)

If nLastKey == 27
   Return
Endif

nTipo := If(aReturn[4]==1,15,18)


//  Processamento. RPTSTATUS monta janela com a regua de processamento.  


RptStatus({|| RunReport(Cabec1,Cabec2,Titulo,nLin) },Titulo)
Return
      



Static Function RunReport(Cabec1,Cabec2,Titulo,nLin)

Local nOrdem

dbSelectArea(cString)
dbSetOrder(1)


//  SETREGUA -> Indica quantos registros serao processados para a regua  

SetRegua(RecCount())

ncontger := 0
ngrupoant := sb1 ->b1_tipo

dbGoTop()
While !EOF()
   
   //  Verifica o cancelamento pelo usuario...                              

   If lAbortPrint
      @nLin,00 PSAY "*** CANCELADO PELO OPERADOR ***"
      Exit
   Endif
 
   //  Impressao do cabecalho do relatorio. . .                             

   If nLin > 55 // Salto de P�gina. Neste caso o formulario tem 55 linhas...
      Cabec(Titulo,Cabec1,Cabec2,NomeProg,Tamanho,nTipo)
      nLin := 8  
      ncontger ++ 
   Endif

   // Coloque aqui a logica da impressao do seu programa...
   // Utilize PSAY para saida na impressora. Por exemplo:  
   
    @nLin,01 PSAY SB1->b1_COD
    @nLin,15 PSAY SB1->b1_DESC
    @nLin,64 PSAY SB1->b1_TIPO
    @nLin,74 PSAY SB1->b1_UM

   nLin := nLin + 1 // Avanca a linha de impressao
   dbSkip() // Avanca o ponteiro do registro no arquivo
   ncontger ++
EndDo

//  Finaliza a execucao do relatorio...                                  

SET DEVICE TO SCREEN

//  Se impressao em disco, chama o gerenciador de impressao...           

If aReturn[5]==1
   dbCommitAll()
   SET PRINTER TO
   OurSpool(wnrel)
Endif

MS_FLUSH()

Return