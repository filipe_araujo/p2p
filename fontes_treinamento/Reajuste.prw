User Function Reajuste()

Local cFormula

dbSelectArea("SM4")
dbSetOrder(1)
dbSeek(xFilial("SM4") + "001")
cFormula := SM4->M4_Formula

dbSelectArea("SB1")
dbGoTop()
While !SB1->(Eof())
   RecLock("SB1")
   SB1->B1_Prv1 := &(cFormula)
   MSUnlock()
   SB1->(dbSkip())
End

MsgInfo("Terminou!")

Return
