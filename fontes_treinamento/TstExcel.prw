//----------------------------------------------------------------------------------------------------------------//
// Exportacao dos dados de uma tela Modelo2 para o Excel.
//----------------------------------------------------------------------------------------------------------------//
User Function TExcel1()

Local aHeader := {}
Local aCols   := {}
Local cNum    := "000019"
 ole_createlink()
If !ApOleClient("MSExcel")
   MsgAlert("Microsoft Excel n�o instalado!")
   Return
EndIf

dbSelectArea("SX3")
dbSetOrder(1)
dbSeek("SC6")

While SX3->X3_Arquivo == "SC6" .And. !SX3->(EOF())

   If X3Uso(SX3->X3_Usado)    .And.;
      cNivel >= SX3->X3_Nivel .And.;
      Trim(SX3->X3_Campo) $ "C6_ITEM/C6_PRODUTO/C6_DESCRI/C6_QTDVEN/C6_VALOR"

      AAdd(aHeader, {Trim(SX3->X3_Titulo),;
                     SX3->X3_Campo       ,;
                     SX3->X3_Picture     ,;
                     SX3->X3_Tamanho     ,;
                     SX3->X3_Decimal     ,;
                     SX3->X3_Valid       ,;
                     SX3->X3_Usado       ,;
                     SX3->X3_Tipo        ,;
                     SX3->X3_Arquivo     ,;
                     SX3->X3_Context})
		
   EndIf
	
   SX3->(dbSkip())
	
End

dbSelectArea("SC6")
dbSetOrder(1)
If !dbSeek(xFilial("SC6") + cNum)
   MsgAlert("Pedido nao encontrado!")
   Return
EndIf

While Trim(SC6->C6_Num) == cNum .And. !SC6->(Eof())
   AAdd(aCols, {SC6->C6_ITEM, SC6->C6_PRODUTO, SC6->C6_DESCRI, SC6->C6_QTDVEN,  SC6->C6_VALOR, .F.})
   SC6->(dbSkip())
End

dbSelectArea("SC5")
dbSetOrder(1)
If !dbSeek(xFilial("SC5") + cNum)
   MsgAlert("Pedido nao encontrado!")
   Return
EndIf

DlgToExcel({ {"CABECALHO", "Pedido Vendas", {"Numero","Cliente","Emissao"}, {SC5->C5_Num,SC5->C5_Cliente,SC5->C5_Emissao}}, {"GETDADOS", "Itens do Pedido", aHeader, aCols} })

// Ou, so a parte da GetDados: DlgToExcel({ {"GETDADOS", "TESTE", aHeader, aCols} })

Return

//----------------------------------------------------------------------------------------------------------------//
// Exportacao de dados para o Excel.
//----------------------------------------------------------------------------------------------------------------//
User Function TExcel2()

Local aCabec := {}
Local aDados := {}

If !ApOleClient("MSExcel")
   MsgAlert("Microsoft Excel n�o instalado!")
   Return
EndIf

aCabec := {"Codigo", "Nome", "Endereco"}

dbSelectArea("SA1")
dbGoTop()

While !SA1->(Eof())

   AAdd(aDados, {SA1->A1_Cod, SA1->A1_Nome, SA1->A1_End})

   SA1->(dbSkip())

End

DlgToExcel({ {"ARRAY", "Exportacao para o Excel", aCabec, aDados} })

Return

//----------------------------------------------------------------------------------------------------------------//
// Exportacao de dados para o Excel.
//----------------------------------------------------------------------------------------------------------------//
User Function TExcel3()

Local oExcel
Local cArq
Local nArq
Local cPath

If !ApOleClient("MSExcel")
   MsgAlert("Microsoft Excel n�o instalado!")
   Return
EndIf

cArq  := CriaTrab(Nil, .F.)
cPath := GetSrvProfString("ROOTPATH", "C:\MP8") + "\DIRDOC\"
nArq  := FCreate(cPath + cArq + ".CSV")

If nArq == -1
   MsgAlert("Nao conseguiu criar o arquivo!")
   Return
EndIf

FWrite(nArq, "Codigo;Nome;Endereco" + Chr(13) + Chr(10))

dbSelectArea("SA1")
dbGoTop()
While !SA1->(Eof())
   FWrite(nArq, SA1->A1_Cod + ";" + SA1->A1_Nome + ";" + SA1->A1_End + Chr(13) + Chr(10))
   SA1->(dbSkip())
End

FClose(nArq)

oExcel := MSExcel():New()
oExcel:WorkBooks:Open(cPath + cArq + ".CSV")
oExcel:SetVisible(.T.)
oExcel:Destroy()

FErase(cPath + cArq + ".CSV")

Return
   
USer function fExcel4()
      
	If !ApOleClient("MSExcel")
	   MsgAlert("Microsoft Excel n�o instalado!")
	   Return
	EndIf
	
	cArq  := "SA1990"
	cPath := GetSrvProfString("ROOTPATH", "C:\MP8") + "\DATA\"
	
	oExcel := MSExcel():New()
	oExcel:WorkBooks:Open(cPath + cArq + ".DBF")
	oExcel:SetVisible(.T.)

Return