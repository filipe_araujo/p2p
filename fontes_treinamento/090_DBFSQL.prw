#Include "PROTHEUS.CH"
#Include "RWMAKE.CH"
#Include "TOPCONN.CH"
#DEFINE ENTER CHR(13)+CHR(10)

User Function fRelRat()

Local cQryCC := ""
Local nTotReg:= 0   

cQryCC := "SELECT * "
cQryCC += " FROM "+	RetSqlName("SA1")
cQryCC += " WHERE A1_FILIAL = '" + xFilial("SA1") + "'"
cQryCC += "   AND D_E_L_E_T_ = ' ' " 

cQryCC := ChangeQuery(cQryCC)

dbUseArea(.T., "TOPCONN", TCGenQry(,,cQryCC), 'TEMP', .F., .T.)			

//TCSETFIELD("TEMPO","ZA_SALDO","N",12,02)
//TCSETFIELD("TEMPO","ZA_DATA","D",8,0)

/*
	//Totalizar a quantidade de registros da tabela.	
	TEMPO->(dbGoTop())					                      
	dbEval({||nTotReg++})

	//Barra de progress�o
	ProcRegua(nTotReg)

	//Vai para o primeiro registro da tabela
	TEMPO->(dbGoTop()) 
	
	While TEMPO->( !EOF() )	
		
        ALERT("Nome: "+TEMPO->ZA_NOME+ " Saldo: "+Str(TEMPO->ZA_SALDO)) //+ " Data: "+dtos(TEMPO->Z1_DATA))
        ALERT("Nome: "+Alltrim(TEMPO->ZA_NOME)+ " Saldo: "+Alltrim(Transform(TEMPO->ZA_SALDO,"@E 999,999,999.99"))) //+ " Data: "+dtoc(TEMPO->ZA_DATA))
		TEMPO->( dbSkip() )

	Enddo  
*/

If !ApOleClient("MSExcel")
   MsgAlert("Microsoft Excel n�o instalado!")
   Return
EndIf	

cCaminho:=GetSrvProfString("ROOTPATH", "C:\") + "\system\"

oExcel := MSExcel():New()
oExcel:WorkBooks:Open(cCaminho+"TEMPO.DBF")
oExcel:SetVisible(.T.)
		
dbCloseArea("TEMPO")

Return
