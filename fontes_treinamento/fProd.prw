#INCLUDE "PROTHEUS.CH"
#INCLUDE "TBICONN.CH"
#include "Ap5Mail.ch"

#DEFINE ENTER CHR(13)+CHR(10)

User Function fProd()
Local cMens := "", cServer, cAccount, cPassword, lConectou, lEnviado, lret, lDisconectou
    RpcClearEnv()
	RpcSetType( 3 )
	RpcSetEnv( "99", "01",,,"CFG",, {"SB1"})
	
	SB1->(dbGotop())
	
	While SB1->(!EOF())
		cMens += SB1->B1_COD  +  " "  +  SB1->B1_DESC +  " " + SB1->B1_TIPO + ENTER		
		SB1->(DBSKIP())
	Enddo
 	
	cServer  := "smtp.advpl.com.br"
	cAccount := "workflow@advpl.com.br"
    cPassword:= "work1234"
	
	CONNECT SMTP SERVER cServer ACCOUNT cAccount PASSWORD cPassword Result lConectou
		
		lOk := MailAuth(cAccount,cPassword)
 		If !lOk 
			lOk := QAGetMail() // Funcao que abre uma janela perguntando o usuario e senha para fazer autenticacao
		EndIf

	If !lConectou
		MSGINFO("NAO CONECTOU AO SMTP")
	Else
	
		SEND MAIL FROM cAccount tO "cris@biale.com.br" 	SUBJECT "Produtos WF";
		BODY cMens 	RESULT lEnviado
			
		If !lEnviado
			_cMensagem := ""
			GET MAIL ERROR _cMensagem
			Alert(_cMensagem)
			lRet := .F.
		Endif
		
		DISCONNECT SMTP SERVER Result lDisConectou
 	Endif

Return