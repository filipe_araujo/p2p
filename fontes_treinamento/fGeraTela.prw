#Include "protheus.CH"
#Include "TOPCONN.CH"
User Function fGeraTela()
Local oDlg, oVar 
Local nVar := 0

	Define MSDialog oDlg Title OemToAnsi("Titulo da janela") From 0,0 To 160,380 Pixel
	
		@05,10 To 50,180 Pixel
		
		@15,20 Say "Colocar aqui a mensagem que quiser" Pixel Of oDlg
		@ 25,20 MSGet oVar Var nVar Picture "@E 999,999.99" Size 50,10 Pixel  Of oDlg
		
		@65,20 Button oBtnOk     Prompt "&Ok"       Size 30,15 Pixel; 
		       Action U_idade(nVar) Of oDlg
		@65,80 Button oBtnCancel Prompt "&Cancelar" Size 30,15 Pixel ;
		       Action ( oDlg:End()) Cancel Of oDlg
		
	Activate MSDialog oDlg Centered

Return 

