// Fun��o de manuten��o do Cadastro de Contas.
// Utiliza a fun��o pre-definida AxCadastro(), que contem todas as funcionalidades: inclus�o, altera��o, exclus�o e pesquisa.
// Parametros que devem ser passados:
//    1) Alias do arquivo a ser editado.
//    2) Titulo da janela.
//    3) Nome da fun��o a ser executada para validar uma exclusao.
//    4) Nome da fun��o a ser executada para validar uma inclusao ou altera��o.

User Function Cad2()

Local cVldAlt := ".T."        // Validacao da inclusao/alteracao.
Local cVldExc := ".T."        // Validacao da exclusao.

AxCadastro("SZB", "Cadastro de Transa��es", cVldExc, cVldAlt) 

//AxCadastro("SA1", "Clientes", "U_DelOk()", "U_COK()", aRotAdic, bPre, bOK, bTTS, bNoTTS, , , aButtons, , )  
           

Return Nil
