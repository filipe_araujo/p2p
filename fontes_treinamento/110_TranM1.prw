#Include "RWMAKE.CH"
#include "protheus.ch"

User Function TranM1()

Private cNomAnt, cTipAnt, nValAnt
Private cAlias    := "SZB"
Private aRotina   := {}
Private lRefresh  := .T.
Private cCadastro := "Transa��o de Dep�sito ou Saque"

AAdd( aRotina, {"Pesquisar" , "AxPesqui", 0, 1} )
AAdd( aRotina, {"Visualizar", "AxVisual", 0, 2} )
AAdd( aRotina, {"Incluir"   , "u_Inclui", 0, 3} ) //AxInclui
AAdd( aRotina, {"Alterar"   , "u_Altera", 0, 4} ) //AxAltera
AAdd( aRotina, {"Excluir"   , "u_Deleta", 0, 5} ) //AxExclui

dbSelectArea(cAlias)
dbOrderNickName("NR_IT")
//dbSetOrder(1)

//mBrowse(6,1,22,75,cAlias)
mBrowse(,,,,cAlias)

Return Nil

//----------------------------------------------------------------------------//
User Function Inclui(cAlias, nRegistro, nOpcao)

Local nConfirmou

nConfirmou := AxInclui(cAlias, nRegistro, nOpcao)

If nConfirmou == 1      // Confirmou a inclusao.

   Begin Transaction

      // Atualiza o saldo.
      dbSelectArea("SZA")
      dbOrderNickName("NOME")
      If dbSeek(xFilial("SZA") + SZB->ZB_Nome)
	      If RecLock("SZA", .F.)
		      If SZB->ZB_Tipo == "D" //DEPOSITO
		         SZA->ZA_Saldo := SZA->ZA_Saldo + SZB->ZB_Valor
		       Else //PAGAMENTO
		         SZA->ZA_Saldo := SZA->ZA_Saldo - SZB->ZB_Valor
		      EndIf
		      MSUnLock()
	      EndIf
      Else
	  		Alert("Registro na tabela saldo nao encontrado.")
	  EndIf
 /*
      If SZA->ZA_Saldo < 0
         If ExistBlock("WFSalNeg")     // Ponto de Entrada.
            // O saldo ficou negativo. Envia um WorkFlow para o aprovador.
            // A resposta do aprovador (SIM ou NAO) sera gravada no campo ZB_Aprov.
            u_WFSalNeg(SZA->ZA_Nome, SZA->ZA_EMail, SZB->ZB_Numero, SZB->ZB_Item, SZB->ZB_Data, SZB->ZB_Hist, SZB->ZB_Valor, SZA->ZA_Saldo)
         EndIf
      EndIf
 */
      // Confirma o numero obtido por GetSXENum() no inic.-padrao do campo ZB_NUMERO.
      ConfirmSX8() //GetSXENum("SZB", "ZB_NUMERO")

   End Transaction

EndIf

Return

//----------------------------------------------------------------------------//
User Function Altera(cAlias, nRegistro, nOpcao)

Local nConfirmou

cNomAnt := SZB->ZB_Nome
cTipAnt := SZB->ZB_Tipo
nValAnt := SZB->ZB_Valor

nConfirmou := AxAltera(cAlias, nRegistro, nOpcao)

If nConfirmou == 1      // Confirmou a alteracao.

   Begin Transaction

      If (SZB->ZB_Nome  <> cNomAnt .Or.;
          SZB->ZB_Tipo  <> cTipAnt .Or.;
          SZB->ZB_Valor <> nValAnt)

         // Desatualiza o movimento anterior.
         dbSelectArea("SZA")
         dbOrderNickName("NOME")
         dbSeek(xFilial("SZA") + cNomAnt)
         RecLock("SZA", .F.)
         If cTipAnt == "D"
            SZA->ZA_Saldo := SZA->ZA_Saldo - nValAnt
          Else
            SZA->ZA_Saldo := SZA->ZA_Saldo + nValAnt
         EndIf
         MSUnLock()

         // Atualiza o novo movimento.
         dbSelectArea("SZA")
         dbOrderNickName("NOME")
         dbSeek(xFilial("SZA") + SZB->ZB_Nome)
         RecLock("SZA", .F.)
         If SZB->ZB_Tipo == "D"
            SZA->ZA_Saldo := SZA->ZA_Saldo + SZB->ZB_Valor
          Else
            SZA->ZA_Saldo := SZA->ZA_Saldo - SZB->ZB_Valor
         EndIf
         MSUnLock()

      EndIf

   End Transaction

EndIf

Return

//----------------------------------------------------------------------------//
User Function Deleta(cAlias, nRegistro, nOpcao)

Local nConfirmou

// Chama a rotina de visualizacao para mostrar o movimento a ser excluido.
nConfirmou := AxVisual(cAlias, nRegistro, 2)

If nConfirmou == 1      // Confirmou a exclusao.

   Begin Transaction

	//ERROS DO PROGRAMA
	// 1 - Se no Seek
	// 2 - Selecao de filial da tabela
	// 3 - RecLock somente se registro estiver disponivel

      // Desatualiza o saldo.
      dbSelectArea("SZA")
      dbOrderNickName("NOME")
      If dbSeek(xFilial("SZA") + SZB->ZB_Nome)
	      If RecLock("SZA", .F.)
		      If SZB->ZB_Tipo == "D"
		         SZA->ZA_Saldo := SZA->ZA_Saldo - SZB->ZB_Valor
		       Else
		         SZA->ZA_Saldo := SZA->ZA_Saldo + SZB->ZB_Valor
		      EndIf
		      MSUnLock()
          EndIf
      EndIf
					
      // Exclui o movimento.
      dbSelectArea(cAlias)
      If RecLock(cAlias, .F.)
      	 dbDelete()
         MSUnLock()
      EndIf

   End Transaction

EndIf

Return
