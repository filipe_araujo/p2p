//----------------------------------------------------------------------------------------------------------------//
// Demonstracao de funcoes usadas no TranM2.PRW (Modelo2):
// CriaVar(), FCount(), FieldName(), FieldPos(), FieldGet() e FieldPut().
//----------------------------------------------------------------------------------------------------------------//

User Function FuncMod2()

Local i

/////////////////////////////////////////////////////////////////////
// Funcao CriaVar()                                                //
/////////////////////////////////////////////////////////////////////

// Criacao de uma variavel qualquer.
// A variavel � criada como Private, pois nao esta sendo declarada.
cNome := "Joao da Silva"

// Cria a variavel 'ZB_Data' privada, com conteudo vazio e tipo igual ao campo SZB->ZB_DATA.
M->ZB_Data := CriaVar("ZB_DATA", .F.)

MsgAlert(M->ZB_Data, "Sem inicializador-padr�o")

// Recria a mesma variavel, inicializando com conteudo carregado a partir do inicializador-padrao (X3_RELACAO).
M->ZB_Data := CriaVar("ZB_DATA", .T.)    // O inicializador-padrao deste campo � 'dDataBase'.

MsgAlert(M->ZB_DATA, "Usando inicializador-padr�o")

/////////////////////////////////////////////////////////////////////
// Funcao FCount()    - retorna a quantidade de campos num arquivo //
//        FieldName() - retorna o nome de um campo do arquivo      //
//        FieldPos()  - retorna a posicao fisica de um campo       //
/////////////////////////////////////////////////////////////////////

/* Campos do SZB:
  ,-----------,----------,------,-------------------------------,
  |   Campo   |   Tipo   | Tam. |     Inicializador padrao      |
  |-----------|----------|------|-------------------------------|
1 | ZB_Filial | Caracter |   2  |                               |
2 | ZB_Nome   | Caracter |  20  |                               |
3 | ZB_Numero | Caracter |   4  | GetSXENum(�SZB�, �ZB_NUMERO�) |
4 | ZB_Item   | Caracter |   2  |                               |
5 | ZB_Data   | Data     |   8  | dDataBase                     |
6 | ZB_Tipo   | Caracter |   1  | �D�                           |
7 | ZB_Hist   | Caracter |  20  |                               |
8 | ZB_Valor  | Numerico | 12,2 |                               |
9 | ZB_Aprov  | Caracter |   3  |                               |
  '-----------'----------'------'-------------------------------'*/

dbSelectArea("SZB")     // Seleciona o arquivo SZB.

MsgAlert("O arq. SZB tem " + Str(FCount()) + " campos!")

For i := 1 To FCount()                      // i      -->      1           2           3      ...     9

    MsgAlert(Str(i) + ": " + FieldName(i))  //        --> "ZB_FILIAL"  "ZB_NOME"  "ZB_NUMERO" ... "ZB_APROV"
    cCampo := FieldName(i)
    M->&(cCampo) := CriaVar(cCampo, .T.)

 // M->&("ZB_NOME")   := CriaVar("ZB_NOME"  , .T.)    ---,
 //                                                      |
 // M->ZB_NOME        := "                    "       <--'

Next

/* Variaveis criadas e seus conteudos:

   ZB_FILIAL   "  "
   ZB_NOME     "                    "
   ZB_NUMERO   "NNNN"   --> ultimo num.+1
   ZB_ITEM     "  "
   ZB_DATA     99/99/99 --> data-base do sistema
   ZB_TIPO     "D"
   ZB_HIST     "                    "
   ZB_VALOR    0.00
   ZB_APROV    "   "
*/

MsgAlert("A posi��o do campo ZB_FILIAL � " + Str(FieldPos("ZB_FILIAL")))
MsgAlert("A posi��o do campo ZB_NUMERO � " + Str(FieldPos("ZB_NUMERO")))
MsgAlert("A posi��o do campo ZB_DATA � "   + Str(FieldPos("ZB_DATA")))

/////////////////////////////////////////////////////////////////////
// Existem duas maneiras de ler e gravar conteudos em campos de    //
// arquivos:                                                       //
//                                                                 //
// 1 - Com os nomes dos campos explicitos no programa.             //
//                                                                 //
//     Ex.: cNumero := SZB->ZB_Numero --> le o conteudo do campo   //
//                                        e atribue a uma variavel.//
//                                                                 //
//          SZB->ZB_Numero := cNumero --> grava o conteudo da var. //
//                                        no campo.                //
//                                                                 //
//     Usando-se esta forma, o programa torna-se espec�fico, ou se-//
//     ja, so serve para este arquivo, pois os nomes do campos es- //
//     tao fixados no programa.                                    //
//                                                                 //
// 2 - Com o uso das funcoes FieldPos(), FieldGet() e FieldPut().  //
//                                                                 //
//     Ex.: cNumero := FieldGet(2) --> le o conteudo do campo nume-//
//                                     ro 2 e atribue 'a variavel. //
//                                                                 //
//          cNumero := FieldGet(FieldPos("ZB_Numero")) --> le o    //
//                                     conteudo do campo, obtendo  //
//                                     a sua posicao pela funcao   //
//                                     FieldPos.                   //
//                                                                 //
//          FieldPut(2, "123456")  --> grava o conteudo no campo   //
//                                     numero 2.                   //
//                                                                 //
//     Desta forma, � possivel escrever programas genericos, que   //
//     funcionam com qualquer arquivo, pois os nomes dos campos    //
//     poderiam estar num dicionario de dados.                     //
/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
// Le e grava conteudos nos campos com os nomes dos campos         //
/////////////////////////////////////////////////////////////////////

dbSelectArea("SZB")
cNumero := SZB->ZB_Numero
dData   := SZB->ZB_Data
nValor  := SZB->ZB_Valor

RecLock("SZB", .T.)     // Inclui um registro.
SZB->ZB_Filial := xFilial("SZB")
SZB->ZB_Numero := GetSX8Num("SZB", "ZB_NUMERO")
SZB->ZB_Data   := Date()
SZB->ZB_Valor  := 1000
ConfirmSX8()

/////////////////////////////////////////////////////////////////////
// Le e grava conteudos nos campos usando as funcoes:              //
// FieldGet() e FieldPut()                                         //
/////////////////////////////////////////////////////////////////////

dbSelectArea("SZB")

// Leitura dos campos usando FieldGet().
cNumero := FieldGet(3) // Campo ZB_Numero
dData   := FieldGet(5) // Campo ZB_Data
nValor  := FieldGet(8) // Campo ZB_Valor

// Leitura dos campos usando FieldGet() e FieldPos().
cNumero := FieldGet(FieldPos("ZB_Numero"))
dData   := FieldGet(FieldPos("ZB_Data"))
nValor  := FieldGet(FieldPos("ZB_Valor"))

// Gravacao dos campos usando FieldPut() e FieldPos(). 
//Begin Trasaction
RecLock("SZB", .T.)     // Inclui um registro.
FieldPut(FieldPos("ZB_Filial"), xFilial("SZB"))
FieldPut(FieldPos("ZB_Numero"), GetSX8Num("SZB", "ZB_NUMERO"))
FieldPut(FieldPos("ZB_Data"), Date())
FieldPut(FieldPos("ZB_Valor"), 1200) 
//End Transaction
ConfirmSX8()


// Gravacao dos campos usando FieldPut() e FieldPos().
RecLock("SZB", .T.)     // Inclui um registro.         
	Replace ZB_Numero With "Oioioi",;
	 ZB_Numero With "Oioioi",;
	 ZB_Numero With "Oioioi",;
	 ZB_Numero With "Oioioi",;
	 ZB_Numero With "Oioioi",;
	 ZB_Numero With "Oioioi" 
ConfirmSX8()	 
                    
Return