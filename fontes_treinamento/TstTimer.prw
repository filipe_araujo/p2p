User Function TstTimer()

Local oSayHora

Define MSDialog oDlg Title "" From 0,0 To 300,300 Pixel

@10,10 Say "Teste Timer" Pixel Of oDlg
@30,10 Say "Hora: " Pixel Of oDlg
@30,30 Say oSayHora Var "" Size 30,10 Pixel Of oDlg
oSayHora:SetText(Time())

DEFINE TIMER oTimer INTERVAL 10 ACTION (oSayHora:SetText(Time())) Of oDlg
Activate Timer oTimer

Activate MSDialog oDlg Centered

Return
