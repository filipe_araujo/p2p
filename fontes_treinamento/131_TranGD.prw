#Include "PROTHEUS.CH"

//----------------------------------------------------------------------------------------------------------------//
// Demonstracao da montagem manual de um GetDados, sem ser pelas funcoes Modelo2 ou Modelo3.
//----------------------------------------------------------------------------------------------------------------//
User Function TranGD()

Private aRotina := {}
Private cCadastro := "Transa��es"

AAdd(aRotina, {"Pesquisar" , "AxPesqui"  , 0, 1})
AAdd(aRotina, {"Visualizar", "u_TGDManut", 0, 2})
AAdd(aRotina, {"Incluir"   , "u_TGDManut", 0, 3})
AAdd(aRotina, {"Alterar"   , "u_TGDManut", 0, 4})
AAdd(aRotina, {"Excluir"   , "u_TGDManut", 0, 5})

dbSelectArea("SZB")
dbOrderNickName("NR_IT")
dbGoTop()

mBrowse(,,,,"SZB")

Return Nil

//----------------------------------------------------------------------------------------------------------------//
User Function TGDManut(cAlias, nReg, nOpc)

Local oDlg, oBtnOk, oBtnVolta
Local oNumero, oNome, oData
Local oGetDados
Local nCols
Local i
Local cLinhaOk := "u_TGDLinOK()"
Local cIniCpos := "+ZB_ITEM"
Local nMax     := 10

Private aHeader := {}                 // Cabecalho de cada coluna da GetDados.
Private aCols   := {}                 // Colunas da GetDados.
Private aAlt    := {} 
Private aAlter  := {}

/////////////////////////////////////////////////////////////////////
// Cria vetor aHeader.                                             //
/////////////////////////////////////////////////////////////////////

dbSelectArea("SX3")
dbSetOrder(1)
dbSeek(cAlias)

While !SX3->(EOF()) .And. SX3->X3_Arquivo == cAlias

   If X3Uso(SX3->X3_Usado)    .And.;                            // O Campo � usado.
      cNivel >= SX3->X3_Nivel .And.;                            // Nivel do Usuario � maior que o Nivel do Campo.
      Trim(SX3->X3_Campo) $ "ZB_ITEM/ZB_TIPO/ZB_HIST/ZB_VALOR"  // Campos que ficarao na GetDados.

      AAdd(aHeader, {Trim(X3Titulo()),;
                     SX3->X3_Campo   ,;
                     SX3->X3_Picture ,;
                     SX3->X3_Tamanho ,;
                     SX3->X3_Decimal ,;
                     SX3->X3_Valid   ,;
                     SX3->X3_Usado   ,;
                     SX3->X3_Tipo    ,;
                     SX3->X3_Arquivo ,;
                     SX3->X3_Context})

   EndIf
   
   If Trim(SX3->X3_Campo) <> "ZB_ITEM"
	   Aadd(aAlter,Trim(SX3->X3_Campo))
   EndIf
   
   SX3->(dbSkip())

End

/////////////////////////////////////////////////////////////////////
// Cria vetor aCols.                                               //
/////////////////////////////////////////////////////////////////////

dbSelectArea(cAlias)
dbSetOrder(1)

If nOpc == 3       // A opcao selecionada � INCLUIR.

   // Atribui � variavel o inicializador padrao do campo.
   M->ZB_Numero := CriaVar("ZB_NUMERO", .T.)
   M->ZB_Nome   := CriaVar("ZB_NOME", .T.)
   M->ZB_Data   := CriaVar("ZB_DATA", .T.)

 Else

   M->ZB_Numero := (cAlias)->ZB_Numero
   M->ZB_Nome   := (cAlias)->ZB_Nome
   M->ZB_Data   := (cAlias)->ZB_Data
   
   dbSelectArea(cAlias)
   dbOrderNickName("NOME_NR_IT")  // ZB_Filial + ZB_Nome + ZB_Numero + ZB_Item
   dbSeek(xFilial(cAlias) + M->ZB_Nome + M->ZB_Numero)

   While !EOF() .And. (cAlias)->(ZB_Filial+ZB_Numero) == xFilial(cAlias) + M->ZB_Numero

       AAdd(aCols, Array(Len(aHeader)+1))   // Cria uma linha vazia em aCols.
       nCols := Len(aCols)

       // Preenche a linha que foi criada com os dados contidos na tabela.       
       For i := 1 To Len(aHeader)
           If aHeader[i][10] == "R"    // O campo � real.
              aCols[nCols][i] := FieldGet(FieldPos(aHeader[i][2]))   // Carrega o conteudo do campo.
            Else                       // O campo � virtual.
              // A funcao CriaVar() le as definicoes do campo no dic.dados e carrega a variavel de acordo com
              // o Inicializador-Padrao, que, se nao foi definido, assume conteudo vazio.
              aCols[nCols][i] := CriaVar(aHeader[i][2], .T.)
           EndIf
       Next
       
       // Cria a ultima coluna para o controle da GetDados: deletado ou nao.
       aCols[nCols][Len(aHeader)+1] := .F.
       
       // Atribui o numero do registro neste vetor para o controle na gravacao.
       AAdd(aAlt, Recno())
       dbSelectArea(cAlias)
       dbSkip()

   End

EndIf

Define MSDialog oDlg Title "Transa��es" From 0,0 To 350,615 Pixel

@03,05 To (oDlg:nClientHeight/2)-33,(oDlg:nClientWidth-10)/2 Pixel Of oDlg

@L:=11,010 Say "Numero:" Pixel Of oDlg
@L-1  ,035 MSGet oNumero Var M->ZB_Numero Size 20,9 Pixel When .F. Of oDlg

@L+=15,010 Say "Nome:" Pixel Of oDlg
@L-1  ,035 MSGet oNome Var M->ZB_Nome F3 "SZA" Size 80,9 Pixel When nOpc == 3 Of oDlg

@L    ,180 Say "Data:" Pixel Of oDlg
@L-1  ,200 MSGet oData Var M->ZB_Data Size 50,9 Pixel When nOpc == 3 Of oDlg
                                                            	
//MsGetDados():New(nTop,nLeft,nBottom,nRight, nOpc, [cLinhaOk], [cTudoOk], [cIniCpos], [lDelete], [aAlter], [uPar1], [lEmpty], [nMax], [cFieldOk], [cSuperDel], [uPar2], [cDelOk], [oWnd])
oGetDados := MSGetDados():New(40, 10, 150, 300, nOpc, cLinhaOk, , cIniCpos, .T. , aAlter , , , nMax)

// ***EXERCICIO*** Inclua um botao que execute uma rotina de gravacao dos dados.

@oDlg:nHeight/2-30,oDlg:nClientWidth/2-35 Button oBtnVolta Prompt "Voltar" Size 30,15 Pixel Action oDlg:End() Cancel Message "Abandona a edi��o" Of oDlg
@oDlg:nHeight/2-30,oDlg:nClientWidth/2-70 Button oBtnVolta Prompt "Gravar" Size 30,15 Pixel Action fGrava(nOpc,oDlg) Cancel Message "Gravar dados" Of oDlg

Activate MSDialog oDlg Centered

Return Nil

Static Function fGrava(nOpc,oDlg)

Do Case
   //Case nOpc == 2//Visualizar
   //     MsgAlert("Op��o 2 selecionada")
   Case nOpc == 3//Incluir
        Processa({|| fIncluir() }, "AGUARDE!!!!!", "Gravando os dados, aguarde...") 
        MsgAlert("Informacaos Gravadas com Sucesso!")    
        oDlg:End()
        ConfirmSX8()
   Case nOpc == 4//Editar
        Processa({|| fEditar()  }, "AGUARDE!!!!!", "Gravando os dados, aguarde...") 
        MsgAlert("Informacoes Editadas com Sucesso!")
        oDlg:End()
   Case nOpc == 5//Excluir     
        Processa({|| fExcluir() }, "AGUARDE!!!!!", "Gravando os dados, aguarde...") 
        MsgAlert("Informacoes Excluidas com Sucesso!")
        oDlg:End()
EndCase

Return Nil 

Static Function fIncluir()

Local nTot := Len(aCols)

//BUSCA TOTAL DE REGISTROS  
ProcRegua(Len(aCols))

//GRAVA DADOS DA ZA
dbSelectArea("SZB")
dbOrderNickName("NR_IT")

BEGIN TRANSACTION
	For i:=1 To Len(aCols) 
		//INCREMENTAR REGUA 
		IncProc("Processando registros "+Alltrim(Str(i)) +" De "+ Alltrim(Str(nTot)))
	  
		//VALIDAR DELETADOS AQUI	
	    If !aCols[i][Len(aHeader)+1]
	    
			//INCLUSAO DE MOVIMENTOS NA ZB
			If RecLock("SZB", .T.)
				SZB->ZB_FILIAL := xFilial("SZB")
				SZB->ZB_NOME   := M->ZB_NOME
				SZB->ZB_NUMERO := M->ZB_NUMERO
				SZB->ZB_DATA   := M->ZB_DATA
				SZB->ZB_ITEM   := aCols[i][1]
				SZB->ZB_TIPO   := aCols[i][2]
				SZB->ZB_HIST   := aCols[i][3]
				SZB->ZB_VALOR  := aCols[i][4]
				MSUnlock()
			EndIf
			
			//ATUALIZA SALDO NA ZA
			dbSelectArea("SZA")
			dbOrderNickName("NOME")
			If dbSeek(xFilial("SZA")+M->ZB_NOME)
				If RecLock("SZA", .F.)
					If aCols[i][2] == "D"
						SZA->ZA_Saldo := SZA->ZA_Saldo + aCols[i][4]
					Else
						SZA->ZA_Saldo := SZA->ZA_Saldo - aCols[i][4]
					EndIf
					MSUnlock()
				EndIf
			Endif
		EndIf
	Next i
END TRANSACTION
	
Return 

Static Function fEditar()   

// INCLUIR UMA NOVA LINHA
// EXCLUIR UMA LINHA EXISTENTE NO BANCO
// EXCLUIR UMA LINHA NAO EXISTENTE NO BANCO
// EDITAR UMA LINHA
// BEGIN TRANSACTION
// IF RECLOCK   
// IF SEEK

Local i
Local y
Local nNrCampo

ProcRegua(Len(aCols))

dbSelectArea("SZB")
dbOrderNickName("NR_IT")

BEGIN TRANSACTION
	For i := 1 To Len(aCols)
		
		If i <= Len(aAlt)
			
			// aAlt contem os Recno() dos registros originais.
			// O usuario pode ter incluido mais registros na GetDados (aCols).
			
			dbSelectArea("SZB")
			dbGoTo(aAlt[i])  // Posiciona no registro.
			
			If aCols[i][Len(aHeader)+1]     // A linha esta deletada.
				
				//ATUALIZA SALDO NA ZA
				dbSelectArea("SZA")
				dbOrderNickName("NOME")
				If dbSeek(xFilial("SZA")+M->ZB_NOME)
					If RecLock("SZA", .F.)
						If aCols[i][2] == "D"
							SZA->ZA_Saldo := SZA->ZA_Saldo - aCols[i][4]
						Else
							SZA->ZA_Saldo := SZA->ZA_Saldo + aCols[i][4]
						EndIf
						MSUnlock()
					EndIf
				Endif
				
				// E depois deleta o registro correspondente.
				If RecLock("SZB", .F.)
				dbDelete()
				MSUnlock()
			EndIf
			
		Else                            // A linha nao esta deletada.
			
			//ATUALIZA SALDO NA ZA
			dbSelectArea("SZA")
			dbOrderNickName("NOME")
			If dbSeek(xFilial("SZA")+M->ZB_NOME)
				If RecLock("SZA", .F.)
					If aCols[i][2] == "D"
						SZA->ZA_Saldo := SZA->ZA_Saldo - aCols[i][4]
					Else
						SZA->ZA_Saldo := SZA->ZA_Saldo + aCols[i][4]
					EndIf
					MSUnlock()
				EndIf
			Endif
			
			//ALTERACAO DE MOVIMENTOS NA ZB
			If RecLock("SZB", .F.)
				SZB->ZB_ITEM   := aCols[i][1]
				SZB->ZB_TIPO   := aCols[i][2]
				SZB->ZB_HIST   := aCols[i][3]
				SZB->ZB_VALOR  := aCols[i][4]
				MSUnlock()
			EndIf
			
			//ATUALIZA SALDO NA ZA
			dbSelectArea("SZA")
			dbOrderNickName("NOME")
			If dbSeek(xFilial("SZA")+M->ZB_NOME)
				If RecLock("SZA", .F.)
					If aCols[i][2] == "D"
						SZA->ZA_Saldo := SZA->ZA_Saldo + aCols[i][4]
					Else
						SZA->ZA_Saldo := SZA->ZA_Saldo - aCols[i][4]
					EndIf
					MSUnlock()
				EndIf
			Endif
			
		EndIf
		
	Else     // Foram incluidas mais linhas na GetDados (aCols), logo, precisam ser incluidas.
		
		If !aCols[i][Len(aHeader)+1]
			
			//INCLUSAO DE MOVIMENTOS NA ZB
			If RecLock("SZB", .T.)
				SZB->ZB_FILIAL := xFilial("SZB")
				SZB->ZB_NOME   := M->ZB_NOME
				SZB->ZB_NUMERO := M->ZB_NUMERO
				SZB->ZB_DATA   := M->ZB_DATA
				SZB->ZB_ITEM   := aCols[i][1]
				SZB->ZB_TIPO   := aCols[i][2]
				SZB->ZB_HIST   := aCols[i][3]
				SZB->ZB_VALOR  := aCols[i][4]
				MSUnlock()
			EndIf
			
			//ATUALIZA SALDO NA ZA
			dbSelectArea("SZA")
			dbOrderNickName("NOME")
			If dbSeek(xFilial("SZA")+M->ZB_NOME)
				If RecLock("SZA", .F.)
					If aCols[i][2] == "D"
						SZA->ZA_Saldo := SZA->ZA_Saldo + aCols[i][4]
					Else
						SZA->ZA_Saldo := SZA->ZA_Saldo - aCols[i][4]
					EndIf
					MSUnlock()
				EndIf
			Endif
					
		EndIf
		
	EndIf
	
Next
END TRANSACTION

Return 

Static Function fExcluir()

Local nTot := Len(aCols)
Local i    := 1

ProcRegua(Len(aCols))

BEGIN TRANSACTION
	dbSelectArea("SZB")
	dbOrderNickName("NOME_NR_IT")
	dbSeek(xFilial("SZB") + M->ZB_Nome + M->ZB_Numero)
	
	While !Eof() .And. SZB->ZB_Filial == xFilial("SZB") .And. SZB->ZB_Numero == M->ZB_Numero
		
		// Nao precisa testar o nome pois numero e' chave prim�ria.
		
		IncProc("Processando registros "+Alltrim(Str(i)) +" De "+ Alltrim(Str(nTot)))
		
		// Desatualiza
		dbSelectArea("SZA")
		IF dbSeek(xFilial()+SZB->ZB_Nome)
			If RecLock("SZA", .F.)
				If SZB->ZB_Tipo == "D"
					SZA->ZA_Saldo := SZA->ZA_Saldo - SZB->ZB_Valor
				Else
					SZA->ZA_Saldo := SZA->ZA_Saldo + SZB->ZB_Valor
				EndIf
				MSUnlock()
			EndIf
		ENDIF
		
		If RecLock("SZB", .F.)
			dbDelete()
			MSUnlock()
		EndIf
		
		dbSelectArea("SZB")
		dbSkip() 
		i++
		
	End
END TRANSACTION
	
Return


//----------------------------------------------------------------------------------------------------------------//
User Function TGDLinOK()

Return .T.

//----------------------------------------------------------------------------------------------------------------//

// ***EXERCICIO*** Desenvolva a rotina de grava�ao que sera executada pelo botao criado acima,
//                 com as respectivas rotinas de Inclusao, Altera�ao e Exclusao.
//
//                 ;-) Dica: estas rotinas existem em 120_TranM2.prw. Podem ser copiadas, necessitando
//                           apenas de alguns pequenos ajustes.
