#Include "protheus.CH"
#Include "TOPCONN.CH"
User Function fGeraTe()
Local oDlg, oVar 
Local nVar1 := 0
Local nVar2 := 0
Local nVar3 := 0
Local nVar4 := 0

	Define MSDialog oDlg Title OemToAnsi("Calcula o Quadrado") From 0,0 To 160,380 Pixel
	
		@05,10 To 60,180 Pixel
		
		@ 15,20 Say "Numero 1" Pixel Of oDlg
		@ 15,50 MSGet oVar Var nVar1 Picture "@E 999,999.99" Size 50,10 Pixel  Of oDlg
		@ 25,20 Say "Numero 2" Pixel Of oDlg
		@ 25,50 MSGet oVar Var nVar2 Picture "@E 999,999.99" Size 50,10 Pixel  Of oDlg
		@ 35,20 Say "Numero 3" Pixel Of oDlg
		@ 35,50 MSGet oVar Var nVar3 Picture "@E 999,999.99" Size 50,10 Pixel  Of oDlg
		@ 45,20 Say "Numero 4" Pixel Of oDlg
		@ 55,50 MSGet oVar Var nVar4 Picture "@E 999,999.99" Size 50,10 Pixel  Of oDlg
		
		@65,20 Button oBtnOk     Prompt "&Ok"       Size 30,15 Pixel; 
		       Action U_quadrado({nVar1,nVar2,nVar3,nVar4}) Of oDlg
		@65,80 Button oBtnCancel Prompt "&Cancelar" Size 30,15 Pixel ;
		       Action ( oDlg:End()) Cancel Of oDlg
		
	Activate MSDialog oDlg Centered

Return 

