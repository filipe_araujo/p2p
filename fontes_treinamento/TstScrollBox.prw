User Function Tela1a()

Local cCpo1 := Space(10)
Local cCpo2 := Space(10)
Local cCpo3 := Space(10)

Define MSDialog oDlg Title "" From 0,0 To 600,600 Pixel

@10,10 Say "Teste Scroll" Pixel Of oDlg

@20,10 ScrollBox oScroll HORIZONTAL VERTICAL BORDER Size 50,100 Pixel Of oDlg

@005,05 Say "Teste" Pixel Of oScroll
@020,05 Say "Teste 1" Pixel Of oScroll
@040,05 Say "Teste 2" Pixel Of oScroll
@040,50 Get cCpo1 Size 100,10 Pixel Of oScroll
@060,05 Say "Teste 3" Pixel Of oScroll
@080,05 Say "Teste 4" Pixel Of oScroll
@080,50 Get cCpo2 Size 100,10 Pixel Of oScroll
@100,05 Say "Teste 5" Pixel Of oScroll
@100,50 Get cCpo3 Size 100,10 Pixel Of oScroll

@130,10 Say "Linha de baixo" Pixel Of oDlg

Activate MSDialog oDlg Centered

Return
