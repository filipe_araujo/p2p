#INCLUDE "RWMAKE.CH"

User Function TranM2()

Private aRotina := {}
Private cCadastro := "Transacoes"

AAdd(aRotina, {"&Pesquisar" , "AxPesqui"  , 0, 1})
AAdd(aRotina, {"&Visualizar", "u_TM2Manut", 0, 2})
AAdd(aRotina, {"&Incluir"   , "u_TM2Manut", 0, 3})
AAdd(aRotina, {"&Alterar"   , "u_TM2Manut", 0, 4})
AAdd(aRotina, {"&Excluir"   , "u_TM2Manut", 0, 5})  
AAdd(aRotina, {"A&provacao" , "u_Tela2"   , 0, 6})
AAdd(aRotina, {"&FuncMod2"  , "u_FuncMod2", 0, 7})

dbSelectArea("SZB")
dbOrderNickName("NR_IT")
dbGoTop()

mBrowse(,,,,"SZB")

Return Nil

//----------------------------------------------------------------------------------------------------------------//
User Function TM2Manut(cAlias, nReg, nOpc)

Local cChave := ""
Local nLin
Local i      := 0
Local lRet   := .F.

// Parametros da funcao Modelo2():
// Modelo2(cT, aC, aR, aCGD, nOp, cLinOk, cAllOK, aGetsGD, bF4, cIniCpos, nMax)

Private cT       := "Digita��o de Dep�sitos" // Titulo.
Private aC       := {}                       // Campos do Enchoice.
Private aR       := {}                       // Campos do Rodape.
Private aCGD     := {}                       // Coordenadas do objeto GetDados.
Private cLinOK   := ""                       // Funcao para validacao de uma linha da GetDados.
Private cAllOK   := "u_TM2TudOK()"           // Funcao para validacao de tudo.
Private aGetsGD  := {}                       // Posi��o para edi��o dos itens (GetDados).
Private bF4      := {|| }                    // Bloco de Codigo para a tecla F4.
Private cIniCpos := "+ZB_ITEM"               // String com o nome dos campos que devem inicializados
                                             // ao pressionar a seta para baixo.
Private nMax     := 99                       // Nr. maximo de linhas na GetDados.
Private aHeader  := {}                       // Cabecalho das colunas da GetDados.
Private aCols    := {}                       // Colunas da GetDados.
Private nCount   := 0
Private bCampo   := {|nField| FieldName(nField)}
Private aAlt     := {}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Cria variaveis de memoria:                                                                     //
// Para cada campo da tabela, cria uma variavel de memoria com o mesmo nome.                      //
// Estas variaveis sao usadas em validacoes e gatilhos que existirem para este arquivo.           //
////////////////////////////////////////////////////////////////////////////////////////////////////
/* Campos do SZB:
  ,-----------,----------,------,-------------------------------,
  |   Campo   |   Tipo   | Tam. |     Inicializador padrao      |
  |-----------|----------|------|-------------------------------|
1 | ZB_Filial | Caracter |   2  |                               |
2 | ZB_Nome   | Caracter |  20  |                               |
3 | ZB_Numero | Caracter |   4  | GetSXENum("SZB", "ZB_NUMERO") |
4 | ZB_Item   | Caracter |   2  |                               |
5 | ZB_Data   | Data     |   8  | dDataBase                     |
6 | ZB_Tipo   | Caracter |   1  | "D"                           |
7 | ZB_Hist   | Caracter |  20  |                               |
8 | ZB_Valor  | Numerico | 12,2 |                               |
9 | ZB_Aprov  | Caracter |   3  |                               |
  '-----------'----------'------'-------------------------------'*/

dbSelectArea(cAlias)

For i := 1 To FCount()       // i      -->      1           2           3      ...     9

    cCampo := FieldName(i)   // cCampo --> "ZB_FILIAL"  "ZB_NOME"  "ZB_NUMERO" ... "ZB_APROV"
    M->&(cCampo) := CriaVar(cCampo, .T.)

 // M->&("ZB_NOME")   := CriaVar("ZB_NOME"  , .T.)    ---,
 //                                                      |
 // M->ZB_NOME        := "                    "       <--'

Next

/* Variaveis criadas e seus conteudos:

   ZB_FILIAL   "  "
   ZB_NOME     "                    "
   ZB_NUMERO   "NNNN"   --> ultimo num.+1
   ZB_ITEM     "  "
   ZB_DATA     99/99/99 --> data-base do sistema
   ZB_TIPO     "D"
   ZB_HIST     "                    "
   ZB_VALOR    0.00
   ZB_APROV    "   "
*/

////////////////////////////////////////////////////////////////////////////////////////////////////
// Cria vetor aHeader.                                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////

dbSelectArea("SX3")
dbSetOrder(1)
dbSeek(cAlias)

While SX3->X3_Arquivo == cAlias .And. !SX3->(EOF())

   If X3Uso(SX3->X3_Usado)    .And.;                            // O Campo � usado.
      cNivel >= SX3->X3_Nivel .And.;                            // Nivel do Usuario >= Nivel do Campo.
      Trim(SX3->X3_Campo) $ "ZB_ITEM/ZB_TIPO/ZB_HIST/ZB_VALOR/ZB_APROV"  // Campos que ficarao na GetDados.

      AAdd(aHeader, {Trim(SX3->X3_Titulo),;
                     SX3->X3_Campo       ,;
                     SX3->X3_Picture     ,;
                     SX3->X3_Tamanho     ,;
                     SX3->X3_Decimal     ,;
                     SX3->X3_Valid       ,;
                     SX3->X3_Usado       ,;
                     SX3->X3_Tipo        ,;
                     SX3->X3_Arquivo     ,;
                     SX3->X3_Context})
		
   EndIf
	
   SX3->(dbSkip())
	
End

/* Estrutura do aHeader:
Cada elemento do aHeader �, por sua vez, um array contendo as
seguintes informacoes sobre cada campo que ir� para a GetDados.
,--------,-------,---------,---------,---------,-------,-------,------,---------,---------,
|   1    |   2   |    3    |    4    |    5    |   6   |   7   |  8   |   9     |   10    |
| Titulo | Campo | Picture | Tamanho | Decimal | Valid | Usado | Tipo | Arquivo | Context |
'--------'-------'---------'---------'---------'-------'-------'------'---------'---------'
,=============================================================================================================================================================================================,
| ,-------------------------------------, ,-------------------------------------, ,-------------------------------------------, ,-----------------------------------------------------------, |
| |"Item","ZB_ITEM",,2,0,,,"C","SZB","R"| |"Tipo","ZB_TIPO",,1,0,,,"C","SZB","R"| |"Historico","ZB_HIST",,20,0,,,"C","SZB","R"| |"Valor","ZB_VALOR","@E 999,999,999.99",12,2,,,"N","SZB","R"| |
| '-------------------------------------' '-------------------------------------' '-------------------------------------------' '-----------------------------------------------------------' |
'============================================================================================================================================================================================='
*/

////////////////////////////////////////////////////////////////////////////////////////////////////
// Cria o vetor aCols: contem os dados dos campos da tabela.                                      //
// Cada linha de aCols � uma linha da GetDados e as colunas s�o as colunas da GetDados.           //
// Se a opcao for INCLUIR, cria o vetor aCols com as caracteristicas de cada campo.               //
// Caso contrario, atribui os dados ao vetor aCols.                                               //
////////////////////////////////////////////////////////////////////////////////////////////////////

If nOpc == 3            // A opcao selecionada � INCLUIR.

   /*         ,=============================================================,
     aHeader  |    1.elem.   2.elem.    3.elem.         4.elem.             |
              |  ,---------,---------,-----------,-------------------,      |
   Titulo   1 |  | Item    | Tipo    | Historico | Valor             |      |
   Campo    2 |  | ZB_ITEM | ZB_TIPO | ZB_HIST   | ZB_VALOR          |      |
   Picture  3 |  |         |         |           | @E 999,999,999.99 |      |
   Tamanho  4 |  | 2       | 1       | 20        | 12                |      |
   Decimal  5 |  | 0       | 0       | 0         | 2                 |      |
   Valid    6 |  |         |         |           |                   |      |
   Usado    7 |  |         |         |           |                   |      |
   Tipo     8 |  | C       | C       | C         | N                 |      |
   Arquivo  9 |  | SZB     | SZB     | SZB       | SZB               |      |
   Context 10 |  | R       | R       | R         | R                 |      |
              |  '---------'---------'-----------'-------------------'      |
              '============================================================='
              ,=============================================================,
              |    [1][1]    [1][2]     [1][3]          [1][4]       [1][5] |
              |  ,---------,---------,-----------,-------------------,---,  |
     aCols[1] |  |         |         |           |                   |   |  |
              |  '---------'---------'-----------'-------------------'---'  |
              '============================================================='*/

   // Como cada elemento de aCols sempre contera um elemento a mais que o aHeader,
   // adiciona em aCols um ARRAY com o "num.elementos de aHeader + 1", ou seja, 5 elementos.
   AAdd(aCols, Array(Len(aHeader)+1))  // aCols[1] --> { Nil, Nil, Nil, Nil, Nil }

   /* Preenche cada elemento desse array, de acordo com o Inicializador-Padrao do Dic.Dados.

   ,===(aCols)=======================================================================================================================================,
   |                                aCols[1]  (Na inclusao, so aCols[1])                                    aCols[2]                                 |
   |   ,---------------,---------------,---------------,---------------,---,   ,---------------,---------------,---------------,---------------,     |
   |   |           1   |           2   |           3   |           4   |   |   |           1   |           2   |           3   |           4   |     |
   |   |  aCols[1][i]  |  aCols[1][i]  |  aCols[1][i]  |  aCols[1][i]  |   |   |  aCols[2][i]  |  aCols[2][i]  |  aCols[2][i]  |  aCols[2][i]  | ... |
   |   '---------------'---------------'---------------'---------------'---'   '---------------'---------------'---------------'---------------'     |
   '================================================================================================================================================='
   ,===(aHeader)=============================================================,
   |             1               2               3               4           |
   |     aHeader[i][2]   aHeader[i][2]   aHeader[i][2]   aHeader[i][2]       |
   |                |               |               |               |        |
   |   ,------------|--,------------|--,------------|--,------------|--,     |
   |   | Item       |  | Tipo       |  | Historico  |  | Valor      |  |     |
   |   | ZB_ITEM <--'  | ZB_TIPO <--'  | ZB_HIST <--'  | ZB_VALOR <-'  |     |
   |   '---------------'---------------'---------------'---------------'     |
   '========================================================================='*/

   For i := 1 To Len(aHeader)
       aCols[1][i] := CriaVar(aHeader[i][2])
   Next
   //  aCols[1][1] := CriaVar("ZB_ITEM" )
   //  aCols[1][2] := CriaVar("ZB_TIPO" )
   //  aCols[1][3] := CriaVar("ZB_HIST" )
   //  aCols[1][4] := CriaVar("ZB_VALOR")

   // Inicializa a ultima coluna para o controle da GetDados: deletado ou nao.
   // aCols[1][5] := .F.
   aCols[1][Len(aHeader)+1] := .F.

   // Inicializa a coluna do ITEM com 01.
   // aCols[1][1] := "01"  <-- teria problema se o usuario alterasse a posicao do
   //                          campo ZB_ITEM no Dic. de Dados. 
   aCols[1][AScan(aHeader, {|x|Trim(x[2])=="ZB_ITEM"})] := "01"

 Else                   // Opcao ALTERAR ou EXCLUIR.

   M->ZB_Numero := (cAlias)->ZB_Numero
   M->ZB_Nome   := (cAlias)->ZB_Nome
   M->ZB_Data   := (cAlias)->ZB_Data

   dbSelectArea(cAlias)
   dbOrderNickName("NOME_NR_IT")  // ZB_Filial + ZB_Nome + ZB_Numero + ZB_Item
   dbSeek(xFilial(cAlias) + M->ZB_Nome + M->ZB_Numero)

   While !EOF() .And. (cAlias)->(ZB_Filial+ZB_Numero) == xFilial(cAlias) + M->ZB_Numero

   /*         ,==================================================================,
     aHeader  |    1.elem.   2.elem.    3.elem.         4.elem.                  |
              |  ,---------,---------,-----------,-------------------,           |
   Titulo   1 |  | Item    | Tipo    | Historico | Valor             |           |
   Campo    2 |  | ZB_ITEM | ZB_TIPO | ZB_HIST   | ZB_VALOR          |           |
   ...        /  /         /         /           /                   /           /
   Context 10 |  | R       | R       | R         | R                 |           |
              |  '---------'---------'-----------'-------------------'           |
              '=================================================================='     aAlt
              ,==================================================================,   ,-------,
              |  ,---------,---------,-----------,-------------------,--------,  |   |       |
     aCols[1] |  | [1][1]  | [1][2]  |  [1][3]   |      [1][4]       | [1][5] |  |   |Recno()| aAlt[1]
              |  '---------'---------'-----------'-------------------'--------'  |   |-------|
              |  ,---------,---------,-----------,-------------------,--------,  |   |       |
     aCols[2] |  | [2][1]  | [2][2]  |  [2][3]   |      [2][4]       | [2][5] |  |   |Recno()| aAlt[2]
              |  '---------'---------'-----------'-------------------'--------'  |   |-------|
              |  ,---------,---------,-----------,-------------------,--------,  |   |       |
     aCols[3] |  | [3][1]  | [3][2]  |  [3][3]   |      [3][4]       | [3][5] |  |   |Recno()| aAlt[3]
              |  '---------'---------'-----------'-------------------'--------'  |   '-------'
              '=================================================================='*/

      // Como cada elemento de aCols sempre contera um elemento a mais que o aHeader,
      // adiciona em aCols um ARRAY com o "num.elementos de aHeader + 1", ou seja, 5 elementos.
      AAdd(aCols, Array(Len(aHeader)+1))  // aCols[1] --> { Nil, Nil, Nil, Nil, Nil }

      nLin := Len(aCols)                  // Nr. da linha que foi criada.

      // Preenche a linha que foi criada com os dados contidos na tabela.
      For i := 1 To Len(aHeader)
          If aHeader[i][10] == "R"                                   // Campo � real.
             aCols[nLin][i] := FieldGet(FieldPos(aHeader[i][2]))     // Carrega o conteudo do campo.
           Else
             // A funcao CriaVar() le as definicoes do campo no dic.dados e carrega a variavel de acordo com
             // o Inicializador-Padrao, que, se nao foi definido, assume conteudo vazio.
             aCols[nLin][i] := CriaVar(aHeader[i][2], .T.)
          EndIf
      Next

      // Inicializa a ultima coluna para o controle da GetDados: deletado ou nao.
      aCols[nLin][Len(aHeader)+1] := .F.

      // Guarda o numero dos registros para controle da gravacao.
      AAdd(aAlt, Recno())

      dbSelectArea(cAlias)
      dbSkip()

   End

EndIf

////////////////////////////////////////////////////////////////////////////////////////////////////
// Cria o vetor Enchoice:                                                                         //
//                                                                                                //
// aC[n][1] = Nome da variavel. Ex.: "ZB_Numero"                                                  //
// aC[n][2] = Array com as coordenadas do Get [x,y], em Pixel.                                    //
// aC[n][3] = Titulo do campo                                                                     //
// aC[n][4] = Picture                                                                             //
// aC[n][5] = Validacao                                                                           //
// aC[n][6] = F3                                                                                  //
// aC[n][7] = Se o campo � editavel, .T., senao .F.                                               //
////////////////////////////////////////////////////////////////////////////////////////////////////

AAdd(aC, { "M->ZB_NUMERO", {20, 10}, "N�mero",    "@!"      ,                                                ,      , .F.       })
AAdd(aC, { "M->ZB_APROV" , {20,200}, "Aprovacao", "@!"      , "M->ZB_APROV =='S  ' .OR. M->ZB_APROV=='N  '"  ,      , (nOpc==3) })
AAdd(aC, { "M->ZB_NOME"  , {35, 10}, "Nome"  ,    "@!"      , "ExistCpo('SZA', ZB_Nome, 1)"                  , "SZA", (nOpc==3) })
AAdd(aC, { "M->ZB_DATA"  , {35,200}, "Data"  ,    "99/99/99",                                                ,      , (nOpc==3) })

// Coordenadas do objeto GetDados.
aCGD := {75,5,128,280}

// Validacao na mudanca de linha quando clicar no botao OK.
cLinOK := "u_TM2LinOK()"

cTitulo := "Transacoes Modelo 2"

////////////////////////////////////////////////////////////////////////////////////////////////////
// Executa a funcao Modelo2().                                                                    //
//                                                                                                //
// Parametros da funcao:                                                                          //
// Modelo2(cT, aC, aR, aCGD, nOp, cLinOk, cAllOK, aGetsGD, bF4, cIniCpos, nMax)                   //
////////////////////////////////////////////////////////////////////////////////////////////////////

lRet := Modelo2(cTitulo, aC, aR, aCGD, nOpc, cLinOK, cAllOK, , , cIniCpos, nMax)

If lRet  // Confirmou (.T.)  Nao confirmou (.F.)

   If      nOpc == 3    // Inclusao
           If MsgYesNo("Confirma a gravacao dos dados?", cTitulo)
              // Cria um dialogo com uma regua de progressao.
              Processa({||TM2Inclu(cAlias)}, cTitulo, "Gravando os dados, aguarde...") 
           EndIf
    ElseIf nOpc == 4    // Alteracao
           If MsgYesNo("Confirma a alteracao dos dados?", cTitulo)
              // Cria um dialogo com uma regua de progressao.
              Processa({||TM2Alter(cAlias)}, cTitulo, "Alterando os dados, aguarde...")
           EndIf
    ElseIf nOpc == 5    // Exclusao
           If MsgYesNo("Confirma a exclusao dos dados?", cTitulo)
              // Cria um dialogo com uma regua de progressao.
              Processa({||TM2Exclu(cAlias)}, cTitulo, "Excluindo os dados, aguarde...")
           EndIf
   EndIf

Else

   RollBackSX8()

EndIf

Return Nil

//----------------------------------------------------------------------------------------------------------------//
Static Function TM2Inclu(cAlias)

Local i
Local y
Local nNrCampo

ProcRegua(Len(aCols))

dbSelectArea(cAlias)
dbOrderNickName("NR_IT")

For i := 1 To Len(aCols)

    IncProc()

    If !aCols[i][Len(aHeader)+1]  // A linha nao esta deletada, logo, deve ser gravada.

       RecLock(cAlias, .T.)

       For y := 1 To Len(aHeader)
           nNrCampo := FieldPos(Trim(aHeader[y][2]))
           FieldPut(nNrCampo, aCols[i][y])
       Next

       (cAlias)->ZB_Filial := xFilial(cAlias)
       (cAlias)->ZB_Nome   := M->ZB_Nome
       (cAlias)->ZB_Numero := M->ZB_Numero
       (cAlias)->ZB_Data   := M->ZB_Data

       MSUnlock()

       // Atualiza saldo
       dbSelectArea("SZA")
       dbOrderNickName("NOME")
       If dbSeek(xFilial("SZA")+SZB->ZB_Nome)
	       RecLock("SZA", .F.)
	       If SZB->ZB_Tipo == "D"
	          SZA->ZA_Saldo := SZA->ZA_Saldo + SZB->ZB_Valor
	       Else
	          SZA->ZA_Saldo := SZA->ZA_Saldo - SZB->ZB_Valor
	       EndIf 
       Endif
       MSUnlock()

    EndIf

Next

ConfirmSX8()

Return Nil

//----------------------------------------------------------------------------------------------------------------//
Static Function TM2Alter(cAlias)

Local i
Local y
Local nNrCampo

ProcRegua(Len(aCols))

dbSelectArea(cAlias)
dbOrderNickName("NR_IT")

For i := 1 To Len(aCols)
	
    If i <= Len(aAlt)
		
       // aAlt contem os Recno() dos registros originais.
       // O usuario pode ter incluido mais registros na GetDados (aCols).
		
       dbSelectArea("SZB")
       dbGoTo(aAlt[i])  // Posiciona no registro.

       If aCols[i][Len(aHeader)+1]     // A linha esta deletada.

          // Desatualiza
          dbSelectArea("SZA")
          IF dbSeek(xFilial("SZA")+SZB->ZB_Nome)
	          RecLock("SZA", .F.)
	          If SZB->ZB_Tipo == "D"
	             SZA->ZA_Saldo := SZA->ZA_Saldo - SZB->ZB_Valor
	          Else
	             SZA->ZA_Saldo := SZA->ZA_Saldo + SZB->ZB_Valor
	          EndIf
          	  MSUnlock()
          ENDIF	

          // E depois deleta o registro correspondente.
          RecLock(cAlias, .F.)
          SZB->(dbDelete())

       Else                            // A linha nao esta deletada.

          // Desatualiza
          dbSelectArea("SZA")
          IF dbSeek(xFilial("SZA")+SZB->ZB_Nome)
	          RecLock("SZA", .F.)
	          If SZB->ZB_Tipo == "D"
	             SZA->ZA_Saldo := SZA->ZA_Saldo - SZB->ZB_Valor
	           Else
	             SZA->ZA_Saldo := SZA->ZA_Saldo + SZB->ZB_Valor
	          EndIf
	          MSUnlock()
           ENDIF
              
          // Regrava os dados.
          RecLock("SZB", .F.)

          For y := 1 To Len(aHeader)
              nNrCampo := FieldPos(Trim(aHeader[y][2]))
              FieldPut(nNrCampo, aCols[i][y]) 
          Next

          SZB->(MSUnlock())

          // Atualiza
          dbSelectArea("SZA")
          IF dbSeek(xFilial("SZA")+SZB->ZB_Nome)
	          RecLock("SZA", .F.)
	          If SZB->ZB_Tipo == "D"
	             SZA->ZA_Saldo := SZA->ZA_Saldo + SZB->ZB_Valor
	           Else
	             SZA->ZA_Saldo := SZA->ZA_Saldo - SZB->ZB_Valor
	          EndIf
	          MSUnlock()
          ENDIF

       EndIf

    Else     // Foram incluidas mais linhas na GetDados (aCols), logo, precisam ser incluidas.

       If !aCols[i][Len(aHeader)+1]

          RecLock(cAlias, .T.)

          For y := 1 To Len(aHeader)
              nNrCampo := FieldPos(Trim(aHeader[y][2]))
              FieldPut(nNrCampo, aCols[i][y])
          Next

          (cAlias)->ZB_Filial := xFilial(cAlias)
          (cAlias)->ZB_Nome   := M->ZB_Nome
          (cAlias)->ZB_Numero := M->ZB_Numero
          (cAlias)->ZB_Data   := M->ZB_Data

          MSUnlock()

          // Atualiza saldo

          dbSelectArea("SZA")
          dbOrderNickName("NOME")
          IF dbSeek(xFilial("SZA")+SZB->ZB_Nome)
	          RecLock("SZA", .F.)
	          If SZB->ZB_Tipo == "D"
	             SZA->ZA_Saldo := SZA->ZA_Saldo + SZB->ZB_Valor
	          Else
	             SZA->ZA_Saldo := SZA->ZA_Saldo - SZB->ZB_Valor
	          EndIf
	          MSUnlock()
          ENDIF

       EndIf

    EndIf

Next

Return Nil

//----------------------------------------------------------------------------------------------------------------//
Static Function TM2Exclu(cAlias)

ProcRegua(Len(aCols))

dbSelectArea(cAlias)
dbOrderNickName("NOME_NR_IT")
dbSeek(xFilial(cAlias) + M->ZB_Nome + M->ZB_Numero)

While !Eof() .And. (cAlias)->ZB_Filial == xFilial(cAlias) .And. (cAlias)->ZB_Numero == M->ZB_Numero

   // Nao precisa testar o nome pois numero e' chave prim�ria.

   IncProc("Aguardando: "+Str(i)+" De: "+Str(Len(aCols)) )

   // Desatualiza
   dbSelectArea("SZA")
   IF dbSeek(xFilial()+SZB->ZB_Nome)
	   RecLock("SZA", .F.)
	   If SZB->ZB_Tipo == "D"
	      SZA->ZA_Saldo := SZA->ZA_Saldo - SZB->ZB_Valor
	   Else
	      SZA->ZA_Saldo := SZA->ZA_Saldo + SZB->ZB_Valor
	   EndIf
	   MSUnlock()    
   ENDIF

   RecLock(cAlias, .F.)
   dbDelete()
   MSUnlock()

   dbSelectArea("SZB")
   dbSkip()

End

Return Nil

//----------------------------------------------------------------------------------------------------------------//
// Valida todas as linhas da GetDados ao confirmar a grava�ao.
//----------------------------------------------------------------------------------------------------------------//
User Function TM2TudOK()

Local lRet := .T.
Local i    := 0
Local nDel := 0

For i := 1 To Len(aCols)
    If aCols[i][Len(aHeader)+1]
       nDel++
    EndIf
Next

If nDel == Len(aCols)
   MsgInfo("Para excluir todos os itens, utilize a op��o EXCLUIR", cTitulo)
   lRet := .F.
EndIf  

If Empty(M->ZB_NOME) // M->ZB_NOME==" "
   MsgInfo("Favor preencher o campo NOME.", cTitulo)
   lRet := .F.
EndIf

If M->ZB_DATA <= ddatabase-1
   MsgInfo("Favor preencher uma data superior ou igual a hoje!", cTitulo)
   lRet := .F.
EndIf

Return lRet

//----------------------------------------------------------------------------------------------------------------//
// Valida a linha atual da GetDados ao teclar seta para baixo ou para cima para a mudan�a de linha.
//----------------------------------------------------------------------------------------------------------------//
User Function TM2LinOK()

Local lRet := .T.
// ***EXERCICIO*** Desenvolva uma rotina para nao aceitar valor acima do parametro MV_VRMAX.

// 1 - Preparar para receber colunas variaveis usando a funcao ascan
// 2 - Colocar em parametros o valor maximo de deposito

If aCols[n][AScan(aHeader, {|x|Trim(x[2])=="ZB_VALOR"})] <= 0
	MsgInfo("O campo valor nao pode ser negativo ou zero.")
	lRet := .F. 
ElseIf aCols[n][AScan(aHeader, {|x|Trim(x[2])=="ZB_VALOR"})] > SuperGetMv( "MV_VRMAX" , 1000 )
	MsgInfo("O campo valor nao pode ser superior a cem mil.")
	lRet := .F.
EndIf

Return lRet
