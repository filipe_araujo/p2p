#Include "APWEBEX.CH"
#Include "TbiConn.ch"

//----------------------------------------------------------------------------//
// Pagina para entrada de dados: Transa��o de Deposito/Saque.
//----------------------------------------------------------------------------//
User Function ASP6()

Local cHtml := ""

WEB EXTENDED INIT cHtml

cHtml += ExecInPage("280_ASP6")

WEB EXTENDED END

Return cHtml

//----------------------------------------------------------------------------//
User Function ASP6Grava()

Local cHtml := ""

WEB EXTENDED INIT cHtml

Prepare Environment Empresa "99" Filial "01" Modulo "ESP" Tables "SA1"

	aCliente:={	{"A1_COD"	,"yyy",Nil},;
				{"A1_LOJA"	,"01",Nil},;
				{"A1_NOME"	,"xxyz",Nil},;
				{"A1_NREDUZ","CLIENTE legal",Nil},;
				{"A1_TIPO"  ,"F",Nil},;
				{"A1_END"   ,"RUA DO ROCIO 123",Nil},;
				{"A1_MUN"   ,"SAO PAULO",Nil},;
				{"A1_EST"   ,"SP",Nil},;
				{"A1_COND"	,"001",Nil}}    
	MSExecAuto({|x,y| mata030(x,y)},aCliente,3)			
	

WEB EXTENDED END

Return cHtml
