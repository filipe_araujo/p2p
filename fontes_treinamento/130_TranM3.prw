#Include "PROTHEUS.CH"

User Function TranM3()

Private aRotina := {}
Private cCadastro := "Transacoes Modelo 3"
Private cAlias1 := "SZA"                    // Alias da Enchoice.
Private cAlias2 := "SZB"                    // Alias da GetDados.

AAdd(aRotina, {"Pesquisar" , "AxPesqui"  , 0, 1})
AAdd(aRotina, {"Visualizar", "u_TM3Manut", 0, 2})
AAdd(aRotina, {"Incluir"   , "u_TM3Manut", 0, 3})
AAdd(aRotina, {"Alterar"   , "u_TM3Manut", 0, 4})
AAdd(aRotina, {"Excluir"   , "u_TM3Manut", 0, 5})
                                               
dbSelectArea(cAlias1)
dbSetOrder(1)//dbOrderNickName("NOME")
dbGoTop()

mBrowse(,,,,cAlias1)

Return Nil

//----------------------------------------------------------------------------------------------------------------//
// Modelo 3.
//----------------------------------------------------------------------------------------------------------------//

User Function TM3Manut(cAlias, nRecno, nOpc)

Local i        := 0
Local cLinOK   := "u_TM3LinOK"
Local cTudoOK  := "u_TM3TudOK"
Local nOpcE    := nOpc
Local nOpcG    := nOpc
Local cFieldOK := "AllwaysTrue"
Local lVirtual := .T.
Local nLinhas  := 99
Local nFreeze  := 0
Local lRet     := .T.

Private aCols        := {}
Private aHeader      := {}
Private aCpoEnchoice := {}
Private aAltEnchoice := {}
Private aAlt         := {}      

// Cria variaveis de memoria dos campos da tabela Pai.
// 1o. parametro: Alias do arquivo --> � case-sensitive, ou seja precisa ser como est� no Dic.Dados.
// 2o. parametro: .T.              --> cria variaveis em branco, preenchendo com o inicializador-padrao.
//                .F.              --> preenche com o conteudo dos campos.
RegToMemory(cAlias1, (nOpc==3))

// Cria variaveis de memoria dos campos da tabela Filho.
RegToMemory(cAlias2, (nOpc==3))

CriaHeader()

CriaCols(nOpc)

lRet := Modelo3(cCadastro, cAlias1, cAlias2, aCpoEnchoice, cLinOK, cTudoOK, nOpcE, nOpcG, cFieldOK, lVirtual, nLinhas, aAltEnchoice, nFreeze)

If lRet
   If      nOpc == 3
           If MsgYesNo("Confirma a grava��o dos dados?", cCadastro)
              Processa({||GrvDados()}, cCadastro, "Gravando os dados, aguarde...")
           EndIf
    ElseIf nOpc == 4
           If MsgYesNo("Confirma a altera��o dos dados?", cCadastro)
              Processa({||AltDados()}, cCadastro, "Alterando os dados, aguarde...")
           EndIf
    ElseIf nOpc == 5
           If MsgYesNo("Confirma a exclus�o dos dados?", cCadastro)
              Processa({||ExcDados()}, cCadastro, "Excluindo os dados, aguarde...")
           EndIf

   EndIf
 Else
   RollBackSX8()
EndIf

Return Nil

//----------------------------------------------------------------------------------------------------------------//
Static Function CriaHeader()

aHeader      := {}
aCpoEnchoice := {}
aAltEnchoice := {}

// aHeader � igual ao do Modelo2.

dbSelectArea("SX3")
dbSetOrder(1)
dbSeek(cAlias2)

While !SX3->(EOF()) .And. SX3->X3_Arquivo == cAlias2

   If X3Uso(SX3->X3_Usado)    .And.;                  // O Campo � usado.
      cNivel >= SX3->X3_Nivel .And.;                  // Nivel do Usuario � maior que o Nivel do Campo.
      Trim(SX3->X3_Campo) $ "ZB_ITEM/ZB_TIPO/ZB_HIST/ZB_VALOR"

      AAdd(aHeader, {Trim(SX3->X3_Titulo),;
                     SX3->X3_Campo       ,;
                     SX3->X3_Picture     ,;
                     SX3->X3_Tamanho     ,;
                     SX3->X3_Decimal     ,;
                     SX3->X3_Valid       ,;
                     SX3->X3_Usado       ,;
                     SX3->X3_Tipo        ,;
                     SX3->X3_Arquivo     ,;
                     SX3->X3_Context})

   EndIf

   SX3->(dbSkip())

End

// Campos da Enchoice.

dbSelectArea("SX3")
dbSetOrder(1)
dbSeek(cAlias1)

While !SX3->(EOF()) .And. SX3->X3_Arquivo == cAlias1

   If X3Uso(SX3->X3_Usado)    .And.;                  // O Campo � usado.
      cNivel >= SX3->X3_Nivel                         // Nivel do Usuario � maior que o Nivel do Campo.

      // Campos da Enchoice.
      AAdd(aCpoEnchoice, X3_Campo)

      // Campos da Enchoice que podem ser editadas.
      // Se tiver algum campo que nao deve ser editado, nao incluir aqui.  
      If !(Trim(SX3->X3_Campo) $ "ZA_NUMERO/ZA_SALDO")
	      AAdd(aAltEnchoice, X3_Campo) 
	  EndIf

   EndIf

   SX3->(dbSkip())

End

Return Nil

//----------------------------------------------------------------------------------------------------------------//
Static Function CriaCols(nOpc)

Local nQtdCpo := 0
Local i       := 0
Local nCols   := 0

nQtdCpo := Len(aHeader)
aCols   := {}
aAlt    := {}

If nOpc == 3       // Inclusao.

   AAdd(aCols, Array(nQtdCpo+1))

   For i := 1 To nQtdCpo
       aCols[1][i] := CriaVar(aHeader[i][2])
   Next
   //Nao esta deletado
   aCols[1][nQtdCpo+1] := .F.

 Else

   dbSelectArea(cAlias2)
   dbSetOrder(1)  // ZB_Filial + ZB_Nome + ZB_Numero + ZB_Item
   dbSeek(xFilial(cAlias2) + (cAlias1)->ZA_Nome)

   While !EOF() .And. (cAlias2)->ZB_Filial == xFilial(cAlias2) .And. (cAlias2)->ZB_Nome== (cAlias1)->ZA_Nome

      AAdd(aCols, Array(nQtdCpo+1))
      nCols++

      For i := 1 To nQtdCpo
          If aHeader[i][10] <> "V"
             aCols[nCols][i] := &(cAlias2 +"->" + aHeader[i][2]) //FieldGet(FieldPos(aHeader[i][2]))
           Else
             aCols[nCols][i] := CriaVar(aHeader[i][2], .T.)
          EndIf
      Next
      //define os deletados
      aCols[nCols][nQtdCpo+1] := .F.
      //serve para edicao, salva o recnu do registro
      AAdd(aAlt, Recno())

      dbSelectArea(cAlias2)
      dbSkip()

   End

EndIf
 
Return Nil
 
//----------------------------------------------------------------------------------------------------------------//
Static Function GrvDados()

Local bCampo := {|nField| Field(nField)}
Local i      := 0
Local y      := 0
Local nItem  := 0  
Local nValTot:= 0

ProcRegua(Len(aCols) + FCount())

// Grava o registro da tabela Pai, obtendo o valor de cada campo
// a partir da var. de memoria correspondente.

dbSelectArea(cAlias1)
RecLock(cAlias1, .T.)
For i := 1 To FCount()
    IncProc()
    If "FILIAL" $ FieldName(i)
       FieldPut(i, xFilial(cAlias1))
     Else
       FieldPut(i, M->&(Eval(bCampo,i)))
    EndIf
Next
MSUnlock()

// Grava os registros da tabela Filho.

dbSelectArea(cAlias2)
dbSetOrder(3)

For i := 1 To Len(aCols)

    IncProc()

	If !aCols[i][Len(aHeader)+1]
		If aCols[i][2]=="D"     
    		nValTot += aCols[i][4] //Val(FieldPos("ZB_VALOR"))
    	else
    		nValTot -= aCols[i][4]
    	endif  
    EndIf
    
    If !aCols[i][Len(aHeader)+1]       // A linha nao esta deletada, logo, pode gravar.

       RecLock(cAlias2, .T.)

		(cAlias2)->ZB_Filial := xFilial(cAlias1)
        (cAlias2)->ZB_Nome   := M->ZA_Nome
        (cAlias2)->ZB_Numero := M->ZA_Numero
        (cAlias2)->ZB_Data   := DATE()

       For y := 1 To Len(aHeader)
           FieldPut(FieldPos(Trim(aHeader[y][2])), aCols[i][y])
       Next

       MSUnlock()

    EndIf

Next

dbSelectArea(cAlias1)
RecLock(cAlias1, .F.)
(cAlias1)->ZA_Saldo := nValTot
MSUnlock()

Return Nil

//----------------------------------------------------------------------------------------------------------------//
Static Function AltDados()

Local i      := 0
Local y      := 0
Local nItem  := 0
Local nValTot:= 0

ProcRegua(Len(aCols) + FCount())

dbSelectArea(cAlias1)
RecLock(cAlias1, .F.)

For i := 1 To FCount()
    IncProc()
    If "FILIAL" $ FieldName(i)
       FieldPut(i, xFilial(cAlias1))
     Else
       FieldPut(i, M->&(fieldname(i)))
    EndIf
Next
MSUnlock()
    
dbSelectArea(cAlias2)
dbSetOrder(3)

nItem := Len(aAlt) + 1

For i := 1 To Len(aCols)
	
	If !aCols[i][Len(aHeader)+1]
		If aCols[i][2]=="D"     
    		nValTot += aCols[i][4] //Val(FieldPos("ZB_VALOR"))
    	else
    		nValTot -= aCols[i][4]
    	endif  
    EndIf
    	
    If i <= Len(aAlt)

       dbGoTo(aAlt[i])
       RecLock(cAlias2, .F.)

       If aCols[i][Len(aHeader)+1]
          dbDelete()
        Else
          For y := 1 To Len(aHeader)
              FieldPut(FieldPos(Trim(aHeader[y][2])), aCols[i][y])
          Next
       EndIf

       MSUnlock()

     Else

       If !aCols[i][Len(aHeader)+1]
          RecLock(cAlias2, .T.)
          For y := 1 To Len(aHeader)
              FieldPut(FieldPos(Trim(aHeader[y][2])), aCols[i][y])
          Next
          (cAlias2)->ZB_Filial := xFilial(cAlias2)
          (cAlias2)->ZB_Numero := (cAlias1)->ZA_Numero
          (cAlias2)->ZB_Nome   := (cAlias1)->ZA_Nome          
          (cAlias2)->ZB_Item   := StrZero(nItem, 2, 0)
          MSUnlock()
          nItem++
       EndIf

    EndIf

Next

dbSelectArea(cAlias1)
RecLock(cAlias1, .F.)
(cAlias1)->ZA_Saldo := nValTot
MSUnlock()

Return Nil

//----------------------------------------------------------------------------------------------------------------//
Static Function ExcDados()

ProcRegua(Len(aCols)+1)   // +1 � por causa da exclusao do arq. de cabe�alho.

dbSelectArea(cAlias2)
dbSetOrder(1)
dbSeek(xFilial(cAlias2) + (cAlias1)->ZA_Nome)

While !EOF() .And. (cAlias2)->Z3_Filial == xFilial(cAlias2) .And. (cAlias2)->ZB_Nome == (cAlias1)->ZA_Nome
   IncProc()
   RecLock(cAlias2, .F.)
   dbDelete()
   MSUnlock()
   dbSkip()
End

dbSelectArea(cAlias1)
dbSetOrder(1)
IndProc()
RecLock(cAlias1, .F.)
dbDelete()
MSUnlock()

Return Nil

//----------------------------------------------------------------------------------------------------------------//
User Function TM3TudOK()

Local lRet := .T.
Local i    := 0
Local nDel := 0

For i := 1 To Len(aCols)
    If aCols[i][Len(aHeader)+1]
       nDel++
    EndIf
Next

If nDel == Len(aCols)
   MsgInfo("Para excluir todos os itens, utilize a op��o EXCLUIR", cCadastro)
   lRet := .F.
EndIf

// ***EXERCICIO*** Reescreva esta valida�ao para nao aceitar produtos repetidos dentro da GetDados.
      
For i:=1 To Len(aCols)
	For j:=1 To Len(aCols)
		If Upper(aCols[i][3])==Upper(aCols[j][3]) .AND. i<>j 
			MsgInfo("Existem produtos repetidos")
			Return .F.
		EndIf
	Next j
Next i


Return lRet
                      
//EXER
// 1 - Tornar os campos de cabe�alho ZA_NUMERO e ZA_SALDO desabilitados para Edi��o
// 2 - O campo ZA_SALDO dever� receber automaticamente o valor total da Acols ZB_SALDO
                     
User Function TM3LinOK

M->ZA_SALDO :=0

For j:=1 To Len(aCols)  
	If aCols[j][2]=="D"
		M->ZA_SALDO += aCols[j][4]
	Else
		M->ZA_SALDO -= aCols[j][4]
	EndIf
Next j                     

MsgInfo("Saldo recalculado! "+chr(13) + chr(10)+"Novo Saldo �: "+Alltrim(Transform(M->ZA_SALDO,"999,999,999.99")))

Return .T.