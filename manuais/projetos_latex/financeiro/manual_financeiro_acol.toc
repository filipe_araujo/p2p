\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Conhecendo o M\IeC {\'o}dulo Financeiro}}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Estrutura do Financeiro}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Fluxo de Caixa}{7}{section.1.2}
\contentsline {section}{\numberline {1.3}Contas a Pagar}{7}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Consultas a Pagar}{9}{subsection.1.3.1}
\contentsline {section}{\numberline {1.4}Contas a Receber}{9}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Consultas a Receber}{10}{subsection.1.4.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Utilizando o Ambiente}}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Acessando o Protheus}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Tela inicial}{12}{section.2.2}
\contentsline {section}{\numberline {2.3}Trocando de M\IeC {\'o}dulo/Empresa/Filial}{13}{section.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Atualiza\IeC {\c c}\IeC {\~o}es}}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Cadastros}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Fornecedores}{16}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Clientes}{17}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Bancos}{18}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Naturezas}{19}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Condi\IeC {\c c}\IeC {\~o}es de Pagamento}{20}{subsection.3.1.5}
\contentsline {section}{\numberline {3.2}Contas a Pagar}{21}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Desdobramento de T\IeC {\'\i }tulo}{23}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Fatura}{24}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Compensa\IeC {\c c}\IeC {\~a}o}{26}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Baixa Manual}{27}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Motivo da Baixa}{28}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Cancelar Baixa}{28}{subsection.3.2.6}
\contentsline {subsection}{\numberline {3.2.7}Baixa Autom\IeC {\'a}tica}{29}{subsection.3.2.7}
\contentsline {subsection}{\numberline {3.2.8}Gerando arquivo CNAB}{30}{subsection.3.2.8}
\contentsline {subsection}{\numberline {3.2.9}Arquivo de Retorno}{32}{subsection.3.2.9}
\contentsline {section}{\numberline {3.3}Contas a Receber}{33}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Incluir um T\IeC {\'\i }tulo Manualmente}{34}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Desdobramento de T\IeC {\'\i }tulo }{35}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Fatura}{37}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Compensa\IeC {\c c}\IeC {\~a}o}{38}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Baixa Manual}{39}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Cancelar a Baixa}{40}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}Baixa Autom\IeC {\'a}tica}{40}{subsection.3.3.7}
\contentsline {subsection}{\numberline {3.3.8}Gerando arquivo CNAB}{41}{subsection.3.3.8}
\contentsline {subsection}{\numberline {3.3.9}Arquivo Retorno}{42}{subsection.3.3.9}
\contentsline {section}{\numberline {3.4}Movimentos Banc\IeC {\'a}rios}{42}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Saldos Banc\IeC {\'a}rios}{43}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Movimento Banc\IeC {\'a}rio}{43}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Transfer\IeC {\^e}ncia}{43}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Movimento direto a pagar e receber}{44}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Concilia\IeC {\c c}\IeC {\~a}o Banc\IeC {\'a}ria}{45}{subsection.3.4.5}
\contentsline {section}{\numberline {3.5}Caixinha}{45}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Inclus\IeC {\~a}o de Caixinha}{46}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Reposi\IeC {\c c}\IeC {\~a}o}{47}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Movimentos}{47}{subsection.3.5.3}
\contentsline {subsection}{\numberline {3.5.4}Prestar Contas}{48}{subsection.3.5.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Consultas}}{51}{chapter.4}
\contentsline {section}{\numberline {4.1}Fluxo de Caixa}{51}{section.4.1}
\contentsline {section}{\numberline {4.2}Fluxo de Caixa por Natureza}{52}{section.4.2}
\contentsline {section}{\numberline {4.3}Posi\IeC {\c c}\IeC {\~a}o do Fornecedor}{53}{section.4.3}
\contentsline {section}{\numberline {4.4}T\IeC {\'\i }tulos a Pagar}{54}{section.4.4}
\contentsline {section}{\numberline {4.5}Posi\IeC {\c c}\IeC {\~a}o do Cliente}{55}{section.4.5}
\contentsline {section}{\numberline {4.6}T\IeC {\'\i }tulos a Receber}{56}{section.4.6}
\contentsline {section}{\numberline {4.7}Consulta Gen\IeC {\'e}rica}{58}{section.4.7}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Relat\IeC {\'o}rios}}{61}{chapter.5}
\contentsline {section}{\numberline {5.1}T\IeC {\'\i }tulos a Pagar}{61}{section.5.1}
\contentsline {section}{\numberline {5.2}Movimento Banc\IeC {\'a}rio}{63}{section.5.2}
\contentsline {section}{\numberline {5.3}Retorno Cnab}{63}{section.5.3}
\contentsline {section}{\numberline {5.4}Caixinha}{64}{section.5.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {Painel}}{65}{chapter.6}
