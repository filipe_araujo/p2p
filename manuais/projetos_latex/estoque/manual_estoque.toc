\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Conhecendo o M\IeC {\'o}dulo Estoque}}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Fluxo Operacional do M\IeC {\'o}dulo}{4}{section.1.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Utilizando o Ambiente}}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Acessando o Protheus}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Tela Inicial}{8}{section.2.2}
\contentsline {section}{\numberline {2.3}Trocando de M\IeC {\'o}dulo/Empresa/Filial}{8}{section.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Atualiza\IeC {\c c}\IeC {\~o}es}}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Cadastros}{11}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Grupo de Produtos}{12}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Unidades de Medida}{13}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Produtos}{13}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Saldos Iniciais}{16}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}Requis. Ao Armaz\IeC {\'e}m}{18}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Solicit. ao Armaz\IeC {\'e}m}{18}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Gera Pr\IeC {\'e}-Resquis\IeC {\~a}o}{19}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Baixa Pr\IeC {\'e}-Resquis\IeC {\~a}o}{19}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Movimentos Internos}{20}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Invent\IeC {\'a}rio}{21}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Transfer\IeC {\^e}ncia Modelo 2}{22}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Internos Mod. 2}{22}{subsection.3.3.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Relat\IeC {\'o}rios}}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}An\IeC {\'a}lise de Movimentos}{25}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Posi\IeC {\c c}\IeC {\~a}o das Sas}{25}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Entradas e Sa\IeC {\'\i }das}{26}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Posi\IeC {\c c}\IeC {\~a}o de Estoque}{28}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Posi\IeC {\c c}\IeC {\~a}o de Estoques}{28}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Saldos Estoques}{29}{subsection.4.2.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Miscel\IeC {\^a}nea}}{31}{chapter.5}
\contentsline {section}{\numberline {5.1}Fechamento}{31}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Saldo Atual p/ Final}{32}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Virada dos Saldos}{32}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Acerto de Invent\IeC {\'a}rio}{33}{section.5.2}
