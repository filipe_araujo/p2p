\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Conhecendo o M\IeC {\'o}dulo Ativo Fixo}}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Fluxo Operacional do Ativo Fixo}{4}{section.1.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Utilizando o Ambiente}}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Acessando o Protheus}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Tela Inicial}{8}{section.2.2}
\contentsline {section}{\numberline {2.3}Trocando de M\IeC {\'o}dulo/Empresa/Filial}{8}{section.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Atualiza\IeC {\c c}\IeC {\~o}es}}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Cadastros}{11}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Ativos}{11}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Plano de Contas}{13}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Descri\IeC {\c c}\IeC {\~a}o Estendida}{14}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Invent\IeC {\'a}rio}{14}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Classifica\IeC {\c c}\IeC {\~a}o de Compras}{16}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}Ap\IeC {\'o}lices de Seguro}{18}{subsection.3.1.6}
\contentsline {subsection}{\numberline {3.1.7}Grupo de Bens}{18}{subsection.3.1.7}
\contentsline {subsection}{\numberline {3.1.8}Cadastro de Pessoas}{20}{subsection.3.1.8}
\contentsline {subsection}{\numberline {3.1.9}Respons\IeC {\'a}veis X Bens}{21}{subsection.3.1.9}
\contentsline {section}{\numberline {3.2}Movimentos}{23}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Baixas}{23}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Transfer\IeC {\^e}ncias}{24}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Simula\IeC {\c c}\IeC {\~a}o}{27}{subsection.3.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Relat\IeC {\'o}rios}}{31}{chapter.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Miscel\IeC {\^a}nea}}{35}{chapter.5}
\contentsline {section}{\numberline {5.1}Processamentos}{35}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}C\IeC {\'a}lculo Mensal}{35}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Desc\IeC {\'a}lculo Mensal}{37}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Bloquei/Desbloqueio de Deprecia\IeC {\c c}\IeC {\~a}o}{38}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Virada Anual}{39}{subsection.5.1.4}
\contentsline {section}{\numberline {5.2}Acertos}{41}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Altera\IeC {\c c}\IeC {\~a}o na taxa de deprecia\IeC {\c c}\IeC {\~a}o}{41}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Verifica\IeC {\c c}\IeC {\~a}o de Integridade}{43}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Refaz Saldos}{43}{subsection.5.2.3}
