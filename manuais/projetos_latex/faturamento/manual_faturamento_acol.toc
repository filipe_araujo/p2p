\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Conhecendo o M\IeC {\'o}dulo Faturamento}}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Estrutura do Faturamento}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Fluxo Operacional do Faturamento}{4}{section.1.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Utilizando o Ambiente}}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Acessando o Protheus}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Tela Inicial}{8}{section.2.2}
\contentsline {section}{\numberline {2.3}Trocando de M\IeC {\'o}dulo/Empresa/Filial}{8}{section.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Atualiza\IeC {\c c}\IeC {\~o}es}}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Pedidos}{11}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Pedidos de Venda}{11}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Faturamento}{13}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Documento de Sa\IeC {\'\i }da}{13}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Nfe Sefaz}{14}{subsection.3.2.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Relat\IeC {\'o}rios}}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}Result. Vendas X Data}{19}{section.4.1}
\contentsline {section}{\numberline {4.2}Result. Vendas X Frete}{19}{section.4.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Miscel\IeC {\^a}nea}}{21}{chapter.5}
\contentsline {section}{\numberline {5.1}Ajuste de Custo}{21}{section.5.1}
