\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Conhecendo o M\IeC {\'o}dulo Compras}}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Fluxo Operacional do Compras}{4}{section.1.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Utilizando o Ambiente}}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Acessando o Protheus}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Tela Inicial}{8}{section.2.2}
\contentsline {section}{\numberline {2.3}Trocando de M\IeC {\'o}dulo/Empresa/Filial}{8}{section.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Atualiza\IeC {\c c}\IeC {\~o}es}}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Cadastros}{11}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Condi\IeC {\c c}\IeC {\~o}es de Pagamento}{11}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Unidades de Medida}{13}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Locais de Estoque}{14}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Grupo de Produtos}{15}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Fornecedores}{16}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}Produtos}{17}{subsection.3.1.6}
\contentsline {subsection}{\numberline {3.1.7}Amarra\IeC {\c c}\IeC {\~a}o Grupo x Fornecedor}{19}{subsection.3.1.7}
\contentsline {subsection}{\numberline {3.1.8}Amarra\IeC {\c c}\IeC {\~a}o Produto x Fornecedor}{20}{subsection.3.1.8}
\contentsline {section}{\numberline {3.2}Solicitar/Cotar}{21}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Solicita\IeC {\c c}\IeC {\~o}es de Compra}{22}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Gera Cota\IeC {\c c}\IeC {\~a}o de Compra}{23}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Atualiza Cota\IeC {\c c}\IeC {\~a}o de Compra}{25}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Analisa Cota\IeC {\c c}\IeC {\~a}o de Compra}{27}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Pedidos de Compra}{28}{section.3.3}
\contentsline {section}{\numberline {3.4}Libera\IeC {\c c}\IeC {\~a}o de Documentos}{31}{section.3.4}
\contentsline {section}{\numberline {3.5}Pr\IeC {\'e}-Nota de Entrada}{33}{section.3.5}
