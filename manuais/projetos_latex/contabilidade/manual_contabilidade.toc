\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Conhecendo o M\IeC {\'o}dulo Contabilidade}}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Estrutura da Contabilidade}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}Fluxo Operacional da Contabilidade}{8}{section.1.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Utilizando o Ambiente}}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Acessando o Protheus}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Tela Inicial}{12}{section.2.2}
\contentsline {section}{\numberline {2.3}Trocando de M\IeC {\'o}dulo/Empresa/Filial}{12}{section.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Atualiza\IeC {\c c}\IeC {\~o}es}}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Cadastros}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Calend\IeC {\'a}rio Cont\IeC {\'a}bil}{16}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Moedas Cont\IeC {\'a}beis}{16}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Moeda X Calend\IeC {\'a}rio}{18}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Contabilista}{19}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}Entidades}{20}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Custos}{21}{subsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.1.1}Centro de Custo}{22}{subsubsection.3.2.1.1}
\contentsline {subsubsection}{\numberline {3.2.1.2}Item Cont\IeC {\'a}bil}{25}{subsubsection.3.2.1.2}
\contentsline {subsubsection}{\numberline {3.2.1.3}C\IeC {\'o}digo Classe de Valor}{28}{subsubsection.3.2.1.3}
\contentsline {subsection}{\numberline {3.2.2}Lan\IeC {\c c}amento Padr\IeC {\~a}o}{31}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Rateio Off-line}{31}{section.3.3}
\contentsline {section}{\numberline {3.4}Lan\IeC {\c c}amentos Cont\IeC {\'a}beis}{33}{section.3.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Consultas}}{37}{chapter.4}
\contentsline {section}{\numberline {4.1}Cadastro Gen\IeC {\'e}rico}{37}{section.4.1}
\contentsline {section}{\numberline {4.2}Raz\IeC {\~a}o Cont\IeC {\'a}bil}{38}{section.4.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Relat\IeC {\'o}rio de Balancete (modelo 1)}}{41}{chapter.5}
